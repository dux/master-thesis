#!/usr/bin/bash
sed  -i -n -e '/\\begin{document}/I,/\\end{document}/I p'  $1 
sed -i '1d;$d' $1
sed -i 's/ \\cite{/~\\cite{/g' $1
sed -i 's/ \\cite\[/~\\cite\[/g' $1
sed -i 's/\n\\cite{/~\\cite{/g' $1
