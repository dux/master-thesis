LYXFILES = lyxfiles
lyxsource = $(wildcard $(LYXFILES)/*.lyx)
FIGURES = figures

all: convertlyx inkscapegraphs compilereport

convertlyx:
	$(foreach var, $(lyxsource), lyx --export-to pdflatex texfiles/$(notdir $(var)).tex $(var); scripts/extract.bash texfiles/$(notdir $(var)).tex;)
clean:
	$(foreach var, $(lyxsource), rm texfiles/$(notdir $(var)).tex;) 

inkscapegraphs:
	inkscape -D $(FIGURES)/preheatingsvg.svg  -o $(FIGURES)/preheatingsvg.pdf --export-latex --export-area-page
	inkscape -D $(FIGURES)/ricci_tensor.svg  -o $(FIGURES)/ricci_tensor.pdf --export-latex --export-area-page

compilereport:
	mkdir -p build/
	pdflatex -synctex=1 -interaction=nonstopmode -aux-directory=build -output-directory=build main.tex
	bibtex build/main.aux
	pdflatex -synctex=1 -interaction=nonstopmode -aux-directory=build -output-directory=build main.tex
	pdflatex -synctex=1 -interaction=nonstopmode -aux-directory=build -output-directory=build main.tex
