
\section{Analytical treatment of Palatini preheating}

We choose to work in the unitary gauge, where the 4 components of
the Higgs $SU(2)$ doublet $H$ in (\ref{eq:jordanframegeneralnonminimalhiggsaction})
can be replaced by a single scalar $h$:
\[
H=\left(\begin{array}{c}
0\\
h/\sqrt{2}
\end{array}\right).
\]
 The resulting action is

\begin{equation}
S=\int d^{4}x\sqrt{-g}\left[\frac{M_{P}^{2}+\xi h^{2}}{2}g^{\mu\nu}R_{\mu\nu}(\Gamma)-\frac{1}{2}g^{\mu\nu}\partial_{\mu}h\partial_{\nu}h-\frac{\lambda h^{4}}{4}\right]\label{eq:unitaryjordanframeaction}
\end{equation}
where again the Ricci tensor does not depend on the metric. This action
is called the Jordan frame action because of the presence of the non-minimal
coupling term $h^{2}R$. It is however easier to analyze the situation
when such a term is not present. Moreover, the difference between
Palatini and metric formulations is made explicit when removing this
term as well. Doing so is the goal of the Weyl transformation of the
next section. 

\subsection{Weyl transformation of the action}

\label{subsec:weyltransfo}It is easy to transform the action (\ref{eq:unitaryjordanframeaction})
in a frame where this coupling is minimal again: the Einstein frame.
Again this transformation is useful because it makes the difference
between Palatini and metric formulations explicit. The alternative
treatment would be the variation with respect to all the fields in
the action -- which would be numerous in the Palatini case since
the connection counts as fields. So, Weyl transforming the action
is easier as it simply consists in performing a local rescaling of
the metric: 
\begin{equation}
g_{\mu\nu}\to\Omega^{-2}(h)g_{\mu\nu}.\label{eq:weyl_transformation_general}
\end{equation}
In the Palatini formulation, the Ricci tensor does not depend on the
metric so applying (\ref{eq:weyl_transformation_general}) on (\ref{eq:unitaryjordanframeaction})
only requires the transformation of the inverse metric and of the
volume form. They are given by
\begin{align}
g^{\mu\nu} & \to\Omega^{2}(h)g^{\mu\nu}\nonumber \\
\sqrt{-g} & \to\Omega^{-d}(h)\sqrt{-g}.
\end{align}
with $d=4$ dimensions. The transformation can now be chosen such
that it eliminates the non-minimal coupling term:\textcolor{black}{{}
$\Omega^{2}(h)=1+\xi h^{2}/M_{P}^{2}$. This yields the following:
\begin{align}
S & =\int d^{4}x\sqrt{-g}\left[\frac{M_{P}^{2}+\xi h^{2}}{2}\Omega^{-2}(h)g^{\mu\nu}R_{\mu\nu}-\frac{1}{2}\Omega^{-2}(h)g^{\mu\nu}\partial_{\mu}h\partial_{\nu}h-\Omega^{-4}(h)\frac{\lambda h^{4}}{4}\right]\nonumber \\
 & =\int d^{4}x\sqrt{-g}\left[\frac{1}{2}M_{P}^{2}g^{\mu\nu}R_{\mu\nu}(\Gamma)-\frac{1}{2}\frac{1}{1+\xi h^{2}/M_{P}^{2}}g^{\mu\nu}\partial_{\mu}h\partial_{\nu}h-\frac{\lambda}{4}\frac{h^{4}}{\big(1+\xi h^{2}/M_{P}^{2}\big)^{2}}\right].\label{eq:einsteinframeactionunitarygaugebeforefieldredef}
\end{align}
There is still a non-canonical kinetic term that needs to be eliminated,
which can be achieved by redefining the main field:}
\begin{equation}
\frac{d\chi}{dh}=\sqrt{\frac{M_{P}^{2}}{M_{P}^{2}+\xi h^{2}}}.\label{eq:dchidh}
\end{equation}
This redefinition readily eliminates the factor in front of the kinetic
term. The replacement also occurs in the potential part however, so
we need to explicit the field redefinition. Upon integration and inversion,
(\ref{eq:dchidh}) becomes

\begin{align}
h(\chi) & =\frac{M_{P}}{\sqrt{\xi}}\sinh\left(\frac{\sqrt{\xi}}{M_{P}}\chi\right),\label{eq:h_of_chi}
\end{align}
which can then be inserted in the potential part of (\ref{eq:einsteinframeactionunitarygaugebeforefieldredef}):
\begin{align}
V_{E}(\chi) & \coloneqq\frac{\lambda}{4}\frac{h(\chi)^{4}}{\big(1+\xi h(\chi)^{2}/M_{P}^{2}\big)^{2}}\nonumber \\
 & =\frac{\lambda M_{P}^{4}}{4\xi^{2}}\tanh^{4}\left(\frac{\sqrt{\xi}\chi}{M_{P}}\right).\label{eq:einsteinframepalatinipotential}
\end{align}
$V_{E}(\chi)$ is called the Einstein frame potential of the Higgs.
The rest of the action has now been made standard: the Ricci scalar
sits alone and the kinetic term of $\chi$ is canonical. Thus all
the important information is contained in this $V_{E}(\chi$), including
the difference with the metric formulation: the metric version of
this $V_{E}(\chi)$ does not have a closed form, but it is quadratic
near its origin whereas (\ref{eq:einsteinframepalatinipotential})
is quartic. The consequences of this difference was investigated by
Rubio and Tomberg~\cite{rubio2019}, we will review their findings
in section \ref{subsec:Mechanism-of-growth-palatini}. But first we
need to lay some groundwork by expliciting the equations of motion
of $\chi$. 

\subsection{Equation of motion of the inflaton}

\label{subsec:palatini_inflaton_eom}The equation of motion of $\chi$
is now given by the Euler-Lagrange equation of the resulting lagrangian
density. Given the metric (\ref{eq:friedmannmetric}), it reads
\begin{equation}
\ddot{\chi}-a^{-2(1-\alpha)}\nabla^{2}\chi+(3-\alpha)\dot{\chi}\mathcal{H}+a^{2\alpha}\frac{dV_{E}(\chi)}{d\chi}=0,\label{eq:chiunitaryeom}
\end{equation}
where $\mathcal{H}=\dot{a}/a$ is the Hubble parameter. This equation,
combined with the Friedmann equations of the scale factor $a$ contains
most of the information needed to understand the dynamics of inflation
and the subsequent preheating. During inflation itself, the regions
where $\chi$ sits at the large values that can drive the expansion
dominate; thus the space can be considered homogeneous and the Laplacian
term can be neglected:
\begin{equation}
\ddot{\chi}+(3-\alpha)\dot{\chi}\mathcal{H}+a^{2\alpha}\frac{dV_{E}(\chi)}{d\chi}=0.\label{eq:chiunitaryeomhomogeneous}
\end{equation}
Now before going further, let us rescale the field, time and space
values in (\ref{eq:chiunitaryeom}) and (\ref{eq:chiunitaryeomhomogeneous}).
This makes the numerical values smaller and the system will thereby
be more stable when dealt with numerically. It is mainly beneficial
because of the large value that the parameter $\xi$ must have in
the Palatini formulation. Following the analysis by Rubio and Tomberg,
we adopt the following parameter values:
\begin{equation}
\lambda=10^{-3}\quad\quad\,\,\xi=10^{7}.\label{eq:palatinilambdaxivalues}
\end{equation}
Under the rescaling
\begin{equation}
\chi\to M_{P}\tilde{\chi}\quad\quad t\to a^{\alpha}\frac{\tilde{t}}{\frac{\sqrt{\lambda}}{2\xi}M_{P}}\quad\quad x\to\frac{\tilde{x}}{\frac{\sqrt{\lambda}}{2\xi}M_{P}},\label{eq:unitaryglobalrescaling}
\end{equation}
the equations (\ref{eq:chiunitaryeom}) and (\ref{eq:chiunitaryeomhomogeneous})
become 
\begin{align}
\ddot{\tilde{\chi}}-a^{-2(1-2\alpha)}\nabla^{2}\tilde{\chi}+(3-\alpha)\dot{\tilde{\chi}}\mathcal{H}+4a^{4\alpha}\sqrt{\xi}\frac{\tanh(\sqrt{\xi}\text{\ensuremath{\tilde{\chi})^{3}}}}{\cosh(\sqrt{\xi}\text{\ensuremath{\tilde{\chi})^{2}}}} & =0,\label{eq:chiunitaryeomrescaled}\\
\mathrm{\mathrm{and\,\,\,\,}}\ddot{\tilde{\chi}}+(3-\alpha)\dot{\tilde{\chi}}\mathcal{H}+4a^{4\alpha}\sqrt{\xi}\frac{\tanh(\sqrt{\xi}\text{\ensuremath{\tilde{\chi})^{3}}}}{\cosh(\sqrt{\xi}\text{\ensuremath{\tilde{\chi})^{2}}}} & =0.\label{eq:chiunitaryeomhomogeneousrescaled}
\end{align}
Note that the dot notation implies a derivative with respect to the
rescaled time when associated with a tilde variable.

Now that this is dealt with, we still need a sufficiently long epoch
where the field $\tilde{\chi}$ slow-rolls if we want inflation to
last for a large enough number of e-folds. To achieve this, $d\tilde{V}_{E}/d\tilde{\chi}$
must be very small at large $\tilde{\chi}$ values. In other words,
$\tilde{V}_{E}$ should be asymptotically flat and fittingly is as
seen in the left part of figure \ref{fig:palatinipotunitarychievolution}.

\begin{figure}
\centering
\input{figures/potentialandtauschioscillates.pgf}

\caption{(\textit{left}) Palatini potential in the Einstein frame and (\textit{right})
evolution of the homogeneous inflaton field evolving according to
(\ref{eq:chiunitaryeomhomogeneousrescaled}) and (\ref{eq:palatinilambdaxivalues}).\label{fig:palatinipotunitarychievolution}}

\end{figure}
The right part of figure \ref{fig:palatinipotunitarychievolution}
shows the numerically integrated evolution of the inflaton field when
it exits its slow-roll phase and enters the oscillatory regime where
preheating normally occurs. We see there a first difference with metric
Higgs preheating: the inflaton pushes into regions of large field
values for many more oscillations than in metric preheating (we will
see what the same plot looks like in metric preheating in section
\ref{subsec:Metric-lattice-results}). The Hubble friction is thereby
less important, meaning that the Universe expands less for a given
number of oscillations than in the metric case. However, all this
only applies when neglecting the backreaction of the perturbations
that will grow very fast as we will see in the next section. 

\subsection{Mechanism of growth of perturbations}

\label{subsec:Mechanism-of-growth-palatini}As extensively discussed
by Rubio and Tomberg, preheating in Higgs Palatini inflation should
mainly occur through excitations in the Higgs field itself -- the
gauge bosons that are the dominant source of perturbations in metric
Higgs inflation here only play a secondary role. Let us repeat parts
of the analysis of Rubio and Tomberg and expand (\ref{eq:chiunitaryeomhomogeneousrescaled})
to first order in some perturbation $\delta\tilde{\chi}(x,t)$ around
the average value of the field $\tilde{\chi}(t)$. Considering only
the first order component we get
\begin{equation}
\ddot{\delta\tilde{\chi}}(x,t)+(3-\alpha)\dot{\delta\tilde{\chi}}(x,t)\mathcal{H}+a^{4\alpha}\frac{d^{2}\tilde{V}_{E}\big(\tilde{\chi}(t)\big)}{d\tilde{\chi}(t)^{2}}\delta\tilde{\chi}(x,t)=0.
\end{equation}
We can now switch to momentum space by inserting $\delta\tilde{\chi}(x,t)\to\delta\tilde{\chi}_{\tilde{k}}(t)e^{i\tilde{k}\tilde{x}}$,
which yields 
\begin{equation}
\ddot{\delta\tilde{\chi}}_{\tilde{k}}(t)+(3-\alpha)\dot{\delta\tilde{\chi}}_{\tilde{k}}(t)\mathcal{\tilde{H}}+a^{4\alpha}\underbrace{\Bigg(\frac{\tilde{k}^{2}}{a^{2}}+\frac{d^{2}\tilde{V}_{E}\big(\tilde{\chi}(t)\big)}{d\tilde{\chi}(t)^{2}}\Bigg)}_{\mathrm{tachyonic\,if\,negative}}\delta\tilde{\chi}_{\tilde{k}}(t)=0.\label{eq:scalarpalatinilinearizedfourier}
\end{equation}
We see that whenever the highlighted term just above becomes negative,
$\delta\tilde{\chi}_{\tilde{k}}(t)$ enters a phase of exponential
growth. This regime is called ``tachyonic'' because it can only
occur when the effective mass $d^{2}\tilde{V}/d\tilde{\chi}^{2}$
of the field becomes negative: that is whenever the inflaton condensate
is strongly accelerated -- on the ridges and valleys of the trajectory
in figure \ref{fig:palatinipotunitarychievolution}. This growth can
only kick in for long modes with small wavenumbers $\tilde{k}$, so
on top of the depletion of the Higgs condensate through this mechanism,
there must be an additional thermalization that will equally populate
the shorter modes as well before the Universe becomes effectively
radiation dominated ($w=1/3$). Rubio and Tomberg reasonably assume
that this second part of the thermalization is instantaneous and only
provide an evaluation of how fast the first phase of preheating is.
To do so, they consider the fastest growing mode $\tilde{k}_{\mathrm{fast}}$
and estimate after how long its energy density will reach 10\% of
that of the background. They obtain:~\cite[eq. 4.27]{rubio2019}
\begin{equation}
n_{\mathrm{oscillations}}\approx1.4,
\end{equation}
that is the inflaton can oscillate 1.4 times before the linearized
analysis significantly breaks down as the perturbations start to backreact
on the inflaton condensate. 

\subsection{Gauge bosons}

\label{subsec:Palatini-Gauge-bosons}Preheating can also occur in
the gauge degrees of freedom of the Higgs. The mechanism at play is
that of parametric resonance (see e.g.~\cite{evangelos2019}). According
to Rubio and Tomberg this is the subdominant preheating mechanism
in Palatini Higgs inflation. We are going to put this to the test
on the lattice -- we therefore need the corresponding equations of
motion. Adding the gauge bosons consists simply in reintroducing the
covariant derivative in (\ref{eq:unitaryjordanframeaction}).

\begin{equation}
S=\int d^{4}x\sqrt{-g}\left[\frac{M_{P}^{2}+\xi h^{2}}{2}g^{\mu\nu}R_{\mu\nu}-\frac{1}{2}g^{\mu\nu}(D_{\mu}h)^{*}D_{\nu}h-\frac{\lambda}{4}h^{4}\right]\label{eq:actionscalarpalatinihiggsjordanframebosons}
\end{equation}
We will neglect the $SU(2)$ gauge structure:
\begin{equation}
D_{\mu}=\partial_{\mu}-\frac{i}{2}gW_{\mu}\label{eq:scalarized_cov_dev}
\end{equation}
such that
\begin{align*}
(D^{\mu}h)^{*}D_{\mu}h & =(\partial^{\mu}+\frac{i}{2}gW^{\mu})h(\partial_{\mu}-\frac{i}{2}gW_{\mu})h\\
 & =\partial^{\mu}h\partial_{\mu}h+\frac{1}{4}g^{2}h^{2}W^{2}\\
 & =\partial^{\mu}h\partial_{\mu}h+m_{W}^{2}W^{2}
\end{align*}
where $m_{W}(h)=\frac{gh}{2}$. Accounting for the fact that the $SU(2)$
gauge structure would really generate 3 gauge bosons:
\begin{equation}
S=\int d^{4}x\sqrt{-g}\left[\frac{M_{P}^{2}+\xi h^{2}}{2}g^{\mu\nu}R_{\mu\nu}-\frac{1}{2}g^{\mu\nu}\partial_{\mu}h\partial_{\nu}h-\frac{\lambda}{4}h^{4}-\frac{1}{2}\sum_{i=1}^{3}m_{i}^{2}(h)W_{i}\right].\label{eq:explicitactionscalarpalatinihiggsjordanframebosons}
\end{equation}
Neglecting the gauge structure and using only a scalarized emulation
of the Higgsed gauge bosons is not ideal. However the distinction
between longitudinal and transversal degrees of freedom is especially
important in metric Higgs preheating, where the longitudinal polarization
of the bosons has effective mass spikes that shoot past the UV of
the theory. (Note that there are ways to regularize this behavior,
for example y adding an $R{{}^2}$ term -- see e.g.~\cite{bezrukov2019,Minxi2019}
-- to the action like in Starobinsky inflation~\cite{Starobinsky:1980te}).
As pointed out by Rubio and Tomberg, nothing this dramatic distinguishes
the two polarizations of the bosons in the Palatini scenario. Therefore
replacing them with scalars seems more reasonable. Furthermore we
will see in chapter \ref{chap:nonminimalhiggs} that not working in
the unitary gauge and using a multi-component Higgs field brings additional
complications. 

Regarding the action (\ref{eq:explicitactionscalarpalatinihiggsjordanframebosons}),
what needs to be done next is again the transformation to the Einstein
frame to get rid of the non-minimal coupling. Using the same procedure
that followed (\ref{eq:weyl_transformation_general}), we get

\begin{align*}
S_{E} & =\int d^{4}x\sqrt{-g}\left[\frac{M_{P}^{2}}{2}g^{\mu\nu}R_{\mu\nu}-\frac{1}{2}\frac{1}{1+\xi h^{2}/M_{P}^{2}}g^{\mu\nu}\partial_{\mu}h\partial_{\nu}h-V_{E}(h)-\sum_{i}V_{i}(h)\right].
\end{align*}
where 
\begin{equation}
\begin{cases}
V_{E}(h) & =\frac{\lambda}{4}\frac{h^{4}}{(1+\xi h^{2}/M_{P}^{2})^{2}}\\
V_{i}(h) & =\frac{1}{2}m_{i}^{2}(h\big)\frac{W_{i}^{2}}{(1+\xi h^{2}/M_{P}^{2})^{2}}
\end{cases}\label{eq:einsteinpotentialspalatinibosonsh}
\end{equation}
Finally, we canonicalize the action with the field redefinition (\ref{eq:dchidh}).
Once the value $h(\chi)$ (\ref{eq:h_of_chi}) is substituted in (\ref{eq:einsteinpotentialspalatinibosonsh}),
we get
\begin{equation}
\begin{cases}
V_{E}(\chi) & =\frac{\lambda}{4\xi^{2}}M_{P}^{4}\tanh^{4}\left(\frac{\sqrt{\xi}\chi}{M_{P}}\right),\\
V_{i}(\chi) & =\frac{g_{i}^{2}}{8\xi}M_{P}^{2}W_{i}^{2}\frac{\tanh^{2}\left(\frac{\sqrt{\xi}\chi}{M_{P}}\right)}{\cosh^{2}\left(\frac{\sqrt{\xi}\chi}{M_{P}}\right)}.
\end{cases}\label{eq:einsteinpotentialschi}
\end{equation}
The total potential in the Einstein frame is the sum 
\begin{equation}
V_{\mathrm{tot}}(\chi)=\frac{\lambda}{4\xi^{2}}M_{P}^{4}\tanh^{4}\left(\frac{\sqrt{\xi}\chi}{M_{P}}\right)+\frac{M_{P}^{2}}{8\xi}\frac{\tanh^{2}\left(\frac{\sqrt{\xi}\chi}{M_{P}}\right)}{\cosh^{2}\left(\frac{\sqrt{\xi}\chi}{M_{P}}\right)}\sum_{i=1}^{3}g_{i}^{2}W_{i}^{2}.\label{eq:toteinsteinpotential}
\end{equation}
Similarly to the case without gauge bosons, this potential has all
the information that we need because the rest of the action is standard. 

\subsection{Friedmann equations for scalars}

\label{subsec:friedmanneqsscalars}The scale factor can be evolved
using the Friedmann equation (\ref{eq:friedmannenergy}). We just
need an expression for the energy density and pressure of a scalar
field. Let us start by calculating the stress-energy tensor of a generic
scalar $\phi$ with potential $V$:

\begin{align}
T_{\mu\nu} & =-\frac{2}{\sqrt{-g}}\frac{\delta(\sqrt{-g}\mathcal{L})}{\delta g^{\mu\nu}}\nonumber \\
 & =-g_{\mu\nu}\mathcal{L}+2\frac{\partial\mathcal{L}}{\partial g^{\mu\nu}}\nonumber \\
 & =\frac{1}{2}g_{\mu\nu}\partial_{\alpha}\phi\partial^{\alpha}\phi+\partial_{\mu}\phi\partial_{\nu}\phi+g_{\mu\nu}V.\label{eq:genericTmunuscalar}
\end{align}
The energy density and pressure can be read off (\ref{eq:genericTmunuscalar})
after an identification with a perfect fluid is made, that is
\begin{equation}
T_{\mu\nu}=pg_{\mu\nu}+(p+\rho)u_{\mu}u_{\nu}.
\end{equation}
In the rest frame, $u_{\mu}=(a^{\alpha},0,0,0)$ and thus
\begin{equation}
\rho=a^{-2\alpha}T_{00}\quad\quad p=\frac{1}{3}a^{-2}\sum_{i}T_{ii},
\end{equation}
which upon calculation gives
\begin{align}
\rho & =\frac{1}{2}a^{-2\alpha}\dot{\phi}^{2}+\frac{1}{2}a^{-2}(\nabla\phi)^{2}+V\label{eq:simplescalardensity}\\
p & =\frac{1}{2}a^{-2\alpha}\dot{\phi}^{2}-\frac{1}{6}a^{-2}(\nabla\phi)^{2}-V\label{eq:simplescalarpressure}
\end{align}
Fortunately, we are going to use \cosmolattice\, to evolve equations
(\ref{eq:friedmannenergy}) and (\ref{eq:chiunitaryeomrescaled})
on the lattice. This new package automates a lot of the process, for
example by calculating the pressure and energy density of not only
scalars, but also different types of gauge fields. For our purpose,
this means that we do not have to worry about implementing (\ref{eq:simplescalardensity}),
(\ref{eq:simplescalarpressure}) or (\ref{eq:friedmannenergy}) because
\cosmolattice\, already does it for us. In fact, as we will see in
the next section, the only information we need to conduct a lattice
study is the Einstein frame potential of the system. 


