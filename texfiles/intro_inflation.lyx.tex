
\section{Inflation}

Cosmic inflation is the most popular solution to the main shortcomings
of the Big Bang cosmology, in which the Universe is initially a homogeneous
radiation dominated soup that is expanding at a rate diminishing with
time. Alone, the Big Bang cosmology suffers from two major problems
\cite{guth81}:
\begin{itemize}
\item the horizon problem,
\item the flatness problem.
\end{itemize}
Adding a period of accelerating expansion (inflation) at very early
times solves these two problems and provides a quantitative explanation
of the large scale structures observed today. 

\subsection{Horizon problem}

The observed CMB is isotropic and homogeneous. The temperature fluctuations
are of order $\mathcal{O}\big(10^{-5}\big)$ in amplitude compared
to the average value. In the Hot Big Bang cosmology, the Universe
only underwent radiation dominated expansion and would show disconnected
patches at the time of last scattering. These patches would be causally
disconnected and thereby would not have been able to thermalize so
homogeneously. It is intuitive to rationalize why an exponential phase
of expansion helps with solving this problem: the patches originate
from the same very small space, where everything was causally connected. 

\subsection{Flatness problem}

Another useful form of the Friedmann equation is the following~\cite[eq. 4.4]{Lozanovlecturesreheating}:
\begin{equation}
\mathcal{H}^{2}=\frac{\rho}{3M_{P}^{2}}-\frac{K}{a^{2}},
\end{equation}
where $\mathcal{H}=\dot{a}/a$ is the Hubble parameter, $M_{P}$ is
the reduced Planck mass and $K\in\{-1,0,1\}$ denotes the type of
curvature. This can be rewritten as follows:
\begin{equation}
\Omega_{K}\equiv-\frac{K}{a^{2}\mathcal{H}^{2}}=1-\frac{\rho}{3M_{P}^{2}\mathcal{H}^{2}}.
\end{equation}
The right hand side is a pure function of time if the Universe is
radiation dominated. Namely we have: $\mathcal{H}\propto t^{-1}$,
$\rho=\rho_{0}a^{-4}$ and $a=a_{0}t^{2/3}$. We get:
\begin{equation}
\Omega_{K}=1-\frac{3\rho_{0}}{4a_{0}^{4}M_{P}^{2}t^{2/3}}.
\end{equation}
So $\Omega_{K}$, the quantity associated with the density of the
curvature, increases with time. It turns out that $\Omega_{K}$ can
be observed today and its value is very small
\begin{equation}
\Omega_{K,\,\mathrm{today}}\lessapprox0.01,
\end{equation}
which means that at earlier times, $\Omega_{K}$ must have had an
extremely fined tuned value close to zero. 

Now inflation provides a way to make this $\Omega_{K}$ arbitrarily
small. Indeed during inflation, the Hubble parameter can be considered
constant and the scale factor evolves exponentially: $a(t)\propto a_{0}\exp(\mathcal{H}t)$.
Reading from the definition of $\Omega_{K}$:
\begin{equation}
\Omega_{K}\equiv-\frac{K}{a^{2}\mathcal{H}^{2}}\sim\frac{K}{a_{0}\exp(\mathcal{H}t)\mathcal{H}^{2}},
\end{equation}
so an exponentially decreasing value, which can be driven arbitrarily
close to 0 by requesting that inflation lasts long enough. Specifically,
it is generally accepted that the Universe must expand by at least
50 e-folds during its inflationary epoch. This requirement is deemed
easy to achieve with the model of inflation considered in this project.
\cite{Remmen2014}

\subsection{Chaotic inflation}

\label{subsec:mechanisms_driving_inflation}There are many proposed
mechanisms able to drive inflation. The one of interest in this project
is chaotic inflation, also called large-field inflation. Consider
a theory combining general relativity and some scalar field $\phi$,
called the inflaton. The action resulting from the combination of
GR and of the scalar $\phi$ looks like this:
\begin{equation}
S=\int d^{4}x\sqrt{-g}\Bigg(\frac{M_{P}^{2}}{2}R+\partial_{\mu}\phi\partial^{\mu}\phi+V(\phi)\Bigg).\label{eq:simplestgraviscalaraction}
\end{equation}
The patches of space that expand are those where $V(\phi)\gg\partial_{\mu}\phi\partial^{\mu}\phi$:
that is where the pressure is negative. These patches will expand
enormously and soon dominate the spatial landscape of the Universe.
This makes the field $\phi$ quite homogeneous as well, since only
the patches with a specific value of $V(\phi)$ will have expanded.
Now, we can calculate the equation of motion of the scalar field assuming
a Friedmann--Lema\^itre--Robertson--Walker (FLRW) background:
\begin{equation}
\ddot{\phi}+3\mathcal{H}\dot{\phi}+V'(\phi)=0.
\end{equation}
 We see that the field $\phi$ is damped by the Hubble friction term
$3\mathcal{H}\dot{\phi}.$ Moreover, if the inflation is to sustain
itself for a sufficient amount of time, then $V'(\phi$) must be very
small such that $\phi$ remains at the value where the potential dominates
for the required amount of time. In this regime, $\ddot{\phi}=0$:
\begin{equation}
\dot{\phi}=-\frac{V'(\phi)}{3\mathcal{H}}.\label{eq:generic_slow_roll_condition}
\end{equation}
This is called the slow-roll of the inflaton field and sets a constaint
on what the potential $V(\phi)$ must look like. 

\section{Higgs as the inflaton}

Combining the Standard Model and GR at tree level can be as simple
as adding the relevant parts of the separate actions together like
done in equation (\ref{eq:simplestgraviscalaraction}). Since we are
interested in a scalar able to drive inflation, we should combine
the scalar sector of the Standard Model with gravity. The resulting
action would read

\begin{equation}
S=\int d^{4}x\sqrt{-g}\left[\frac{M_{P}^{2}}{2}g^{\mu\nu}R_{\mu\nu}-g^{\mu\nu}(D_{\mu}H)^{\dagger}D_{\nu}H+m^{2}H^{\dagger}H-\lambda(H^{\dagger}H)^{2}\right].
\end{equation}
This type of model does not work as it generates perturbations in
the CMB larger than the observation~\cite{Guth1982}. The fix is however
straightforward~\cite{shaposhnikov2008}: when combining GR and the
scalar part of the Standard Model, there is a new dimension 4 operator
which does not exist in GR or the Standard Model taken separately:
$\xi H^{\dagger}HR$, with $\xi$ a constant. Higher dimentional terms
can be added and lead to a successful Higgs inflation as well, see
e.g.~\cite{Stelios21}. In this project we focus on the simplest model
consistent with observation, that is 
\begin{equation}
S=\int d^{4}x\sqrt{-g}\left[\frac{M_{P}^{2}+2\xi H^{\dagger}H}{2}g^{\mu\nu}R_{\mu\nu}-g^{\mu\nu}(D_{\mu}H)^{\dagger}D_{\nu}H+m^{2}H^{\dagger}H-\lambda(H^{\dagger}H)^{2}\right].
\end{equation}
The covariant derivative is given by
\begin{equation}
D_{\mu}=\partial_{\mu}-\frac{i}{2}gW_{\mu}^{a}\sigma^{a}-\frac{i}{2}g'B_{\mu}
\end{equation}
where $\sigma^{a}$ are the Pauli matrices, the $W_{\mu}^{a}$ are
the weak isospin bosons and $B_{\mu}$ is the weak hypercharge boson.
If we neglect $m$, the Higgs does not take a vacuum expectation value:
let us do that from now on ($m$ is much smaller than the scale of
inflation):

\begin{equation}
S=\int d^{4}x\sqrt{-g}\left[\frac{M_{P}^{2}+2\xi H^{\dagger}H}{2}g^{\mu\nu}R_{\mu\nu}-g^{\mu\nu}(D_{\mu}H)^{\dagger}D_{\nu}H-\lambda(H^{\dagger}H)^{2}\right].\label{eq:jordanframegeneralnonminimalhiggsaction}
\end{equation}


\section{Preheating}

The inclusion of an inflationary epoch in the history of the Universe
is problematic at first sight because the subsequent epochs, in particular
Big Bang nucleosynthesis, are radiation dominated. Indeed inflation
makes the Universe very homogeneous and therefore cold: there needs
to be a mechanism that heats up this cold condensate to a radiation
dominated soup: this mechanism is called reheating or preheating.
The former term implies that the Universe was previously hot but this
has been a subject of debate~\cite{linde1990}. Another convention
is that ``reheating'' has a perturbative connotation and happens
at a later stage of the process, whereas ``preheating'' encompasses
the non-perturbative mechanisms that are usually effective earlier
and produce excitations that are still far from thermal equilibrium~\cite{Kofman1997}.
Non-perturbative preheating usually goes together with non-linear
dynamics and high occupation numbers, making it an interplay of classical
fields. This together with the ongoing increase of the available computational
power makes classical lattice studies especially adequate to capture
the dynamics of preheating. In this work, we will neglect the perturbative
decay of the condensate and focus solely on the non-perturbative mechanisms. 

The preheating that follows chaotic inflation happens schematically
as the inflaton starts oscillating in its potential well (figure \ref{fig:preheatingsvg}).
This breathing mode contains the entirety of the energy of the Universe
and thereby oscillates with a lot of inertia. It is this oscillation
that drives the non-perturbative channels of generation of excitations. 

\begin{figure}
\begin{minipage}[t]{0.38\columnwidth}%
\centering
\import{figures/}{preheatingsvg.pdf_tex}\caption{Illustration of the inflaton exiting its slow-roll phase and starting
to oscillate in its potential well. \label{fig:preheatingsvg}}
%
\end{minipage}\hfill{}%
\begin{minipage}[t]{0.55\columnwidth}%
\centering
\import{figures/}{ricci_tensor.pdf_tex}\caption{Geometric interpretation of equation (\ref{eq:geometricriemanntensor}).
Some vector \textcolor{red}{$Z$} is parallel transported around a
closed loop, the local curvature is encoded in how much \textcolor{red}{$Z$}
changes. \label{fig:riccigeometrysvg}}
%
\end{minipage}
\end{figure}


\section{Palatini formulation of gravity}

Originally, GR imposes two constraints that completely define the
connection. In coordinates, the connection $\Gamma_{\mu\nu}^{\alpha}$
\begin{itemize}
\item should be torsion-free: $\Gamma_{\mu\nu}^{\alpha}=\Gamma_{\nu\mu}^{\alpha}$,
\item needs to be metric compatible, that is $\nabla_{\alpha}g_{\mu\nu}=\partial_{\alpha}g_{\mu\nu}-\Gamma_{\alpha\mu}^{\beta}g_{\beta\nu}-\Gamma_{\alpha\nu}^{\beta}g_{\mu\beta}=0.$
\end{itemize}
Requesting the above completely defines the expression of the connection
in terms of the metric, yielding the Levi-Civita connection expressed
with the Christoffel symbols
\begin{equation}
\text{\ensuremath{\Gamma_{\mu\nu}^{\alpha}=\frac{1}{2}g^{\alpha\rho}\big(g_{\mu\rho,\nu}+g_{\rho\nu,\mu}-g_{\mu\nu,\rho}\big)}.}\label{eq:christoffelsymbols}
\end{equation}
This is called the metric formulation because the only degrees of
freedom are in the metric $g_{\mu\nu}$. In the Palatini formulation
on the other hand, the metric compatibility condition is not enforced:
the Christoffel symbols are just extra fields. This approach is not
new~\cite{Ferraris1982}, and it turns out that the metric compatibility
is dynamically recovered. Indeed, at the exception of the York-Gibbons-Hawking
boundary term which only appears in the metric formulation, the variation
of the Einstein-Hilbert action with respect to the metric yields the
same thing in both cases and results in the usual field equations:
\begin{equation}
R_{\mu\nu}-\frac{1}{2}g_{\mu\nu}R=0.
\end{equation}
The variation with respect to the metric is very simple in the Palatini
case, because the Ricci tensor does not directly depend on the metric.
Indeed, the Riemann tensor only depends on the connection (see figure
\ref{fig:riccigeometrysvg}):
\begin{equation}
R({\color{blue}X},{\color{purple}Y}){\color{red}Z}=\nabla_{{\color{blue}X}}\nabla_{{\color{purple}Y}}{\color{red}Z}-\nabla_{{\color{purple}Y}}\nabla_{{\color{blue}X}}{\color{red}Z}-\nabla_{{\color{orange}[X,Y]}}{\color{red}Z},\label{eq:geometricriemanntensor}
\end{equation}
and thereby so does the Ricci tensor. Still in the Palatini case,
varying the Einstein-Hilbert action with respect to the connection
fixes the metric compatibility condition again. Therefore, the Palatini
and metric formulations give perfectly equivalent predictions in the
conventional tests of GR and the question of which is the correct
formulation can seem arcane. The correspondence between the two formulations
stops however when additional fields are coupled to the Ricci scalar
(nonminimal coupling) just like in action (\ref{eq:jordanframegeneralnonminimalhiggsaction}).

In this project, the practical application will be found again in
the fact that the Ricci tensor does not depend on the metric. This
will induce a difference between metric and Palatini when performing
a Weyl transformation on the action (\ref{eq:jordanframegeneralnonminimalhiggsaction})
as we will see in section \ref{subsec:weyltransfo}. 


