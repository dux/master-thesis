\label{chap:nonminimalhiggs}Until now we studied the situation in
the unitary gauge of the Higgs. The field thereby had only one degree
of freedom that could be excited: its norm. What would happen to the
condensate though, if there were extra degrees of freedom? The Higgs
is normally a $SU(2)$ doublet and has 4 components. To simplify things
slightly, we will replace the $SU(2)$ doublet $H$ with a complex
number. 

We can learn from this simplified model because the field will still
have 2 degrees of freedom: its norm as before, but also a phase. According
to Ema~\cite{Ema2017} excitations appear violently in the phase of
a gauged ~$U(1)$ during metric preheating. This is due however to
a coupling to the effective mass of the longitudinal gauge bosons
which has a spike-like behavior in metric preheating. We can expect
that it will not be the case or at least less important in the Palatini
scenario because
\begin{itemize}
\item preheating occurs mainly through tachyonic excitations,
\item this mass spike does not exist.
\end{itemize}
It is however not immediately clear whether the phase degree of freedom
can undergo an excitation through a mechanism similar to the one acting
on the norm. We will then again focus on a $U(1)$ Higgs alone, without
its gauge bosons, e.g. replace the covariant derivatives of (\ref{eq:jordanframegeneralnonminimalhiggsaction})
with normal ones:

\begin{equation}
S=\int d^{4}x\sqrt{-g}\left[\frac{M_{P}^{2}+2\xi\vert H\vert^{2}}{2}g^{\mu\nu}R_{\mu\nu}-g^{\mu\nu}(\partial_{\mu}H)^{*}\partial_{\nu}H-\lambda\vert H\vert^{4}\right].
\end{equation}
In the following section we will calculate some dynamical equations
of interest for this system. We will then cover the numerical problems
that appear when trying to integrate these equations on a lattice,
and finally we will present some results regarding the evolution of
the phase degree of freedom. 

\section{Obtaining the dynamical equations\label{sec:obtaining_dynamical_eqs_nonminimal}}

\subsection{Einstein frame action}

As in the unitary gauge, let us rescale the metric to eliminate the
nonminimal coupling term. Transform to the Einstein frame using a
conformal rescaling ($R_{\mu\nu}$ depends only on the connection
in the Palatini formulation and thereby is left untouched):
\begin{equation}
S_{E}=\int d^{4}x\sqrt{-g}\left[\frac{M_{P}^{2}+\xi\vert H\vert^{2}}{2}\Omega^{-2}g^{\mu\nu}R_{\mu\nu}-\Omega^{-2}\partial^{\mu}H^{*}\partial_{\mu}H-\lambda\Omega^{-4}\vert H\vert^{4}\right].
\end{equation}
with conformal factor $\Omega^{2}=1+2\xi\vert H\vert^{2}/M_{P}^{2}$
one gets

\begin{align}
S_{E} & =\int d^{4}x\sqrt{-g}\left[\frac{1}{2}M_{P}^{2}g^{\mu\nu}R_{\mu\nu}(\Gamma)-\frac{g^{\mu\nu}\partial_{\mu}H^{*}\partial_{\nu}H}{1+2\xi\vert H\vert^{2}/M_{P}^{2}}-\frac{\lambda\vert H\vert^{4}}{(1+2\xi\vert H\vert^{2}/M_{P}^{2})^{2}}\right].\label{eq:action_einstein_frame}
\end{align}
This time, the kinetic term cannot be canonicalized through a field
transformation, which makes everything a bit more complicated. The
usual expressions for the Friedmann equations, energy momentum tensor
and even the equations of motion of the field itself do not hold anymore.
We have to compute them again by directly varying the corresponding
quantities in the Einstein frame action. 

\subsection{Energy-momentum tensor}

Since we will exclusively work in the Einstein Frame, we start from
(\ref{eq:action_einstein_frame}) and calculate directly.
\begin{align}
T_{\mu\nu} & =-\frac{2}{\sqrt{-g}}\frac{\delta(\sqrt{-g}\mathcal{L})}{\delta g^{\mu\nu}}\nonumber \\
 & =-g_{\mu\nu}\mathcal{L}+2\frac{\partial\mathcal{L}}{\partial g^{\mu\nu}}\nonumber \\
 & =M_{P}^{2}\big(R_{\mu\nu}-\frac{1}{2}g_{\mu\nu}R\big)-\frac{2\partial_{\mu}H^{*}\partial_{\nu}H-g_{\mu\nu}\partial_{\rho}H^{*}\partial^{\rho}H}{1+2\xi\vert H\vert^{2}/M_{P}^{2}}+\frac{g_{\mu\nu}\lambda\vert H\vert^{4}}{(1+2\xi\vert H\vert^{2}/M_{P}^{2})^{2}}
\end{align}
The background is still FLRW so the metric and Ricci tensor can be
explicited.
\begin{align}
g_{00} & =-a^{2\alpha}\quad\quad R_{00}=3\Big(\frac{\ddot{a}}{a}-3\alpha\mathbb{\mathcal{H}}^{2}\Big)\nonumber \\
g_{ii} & =a^{2}\quad\quad\quad\,\,\,R_{ii}=a^{-2\alpha}\big((\alpha-2)\dot{a}^{2}-a\,\ddot{a}\big)\\
R & =-6a^{-2-2\alpha}\big((1-\alpha)\dot{a}^{2}+a\,\ddot{a}\big)\nonumber 
\end{align}
with $\mathbb{\mathcal{H}}$ the Hubble constant. Finally, the energy
density is
\begin{multline}
T_{00}=3M_{P}^{2}\Bigg(\Big(\frac{\ddot{a}}{a}-3\alpha\mathbb{\mathcal{H}}^{2}\Big)+a^{-2\alpha}\Big(\frac{\ddot{a}}{a}+(1-\alpha)\mathcal{H}^{2}\Big)\Bigg)\\
+\frac{1}{1+2\xi\vert H\vert^{2}/M_{P}^{2}}\Big(\vert\dot{H}\vert^{2}+a^{-2+2\alpha}\vert\nabla H\vert^{2}\Big)\\
+a^{2\alpha}\frac{\lambda\vert H\vert^{4}}{(1+2\xi\vert H\vert^{2}/M_{P}^{2})^{2}}.\label{eq:nonunitaryenergy}
\end{multline}
We will adopt the same rescaling as in the unitary case:
\begin{equation}
H\to M_{P}\tilde{H}\quad\quad t\to a^{\alpha}\frac{\tilde{t}}{\frac{\sqrt{\lambda}}{2\xi}M_{P}}\quad\quad x\to\frac{\tilde{x}}{\frac{\sqrt{\lambda}}{2\xi}M_{P}},\label{eq:nonunitaryglobalrescaling}
\end{equation}
under which the energy (\ref{eq:nonunitaryenergy}) reads

\begin{multline}
\tilde{T}_{00}=3a^{-2\alpha}\Bigg(\Big(\frac{\ddot{\tilde{a}}}{a}-3\alpha\mathbb{\mathcal{\tilde{H}}}^{2}\Big)+a^{-2\alpha}\Big(\frac{\ddot{\tilde{a}}}{a}+(1-\alpha)\tilde{\mathcal{H}}^{2}\Big)\Bigg)\\
+a^{-2\alpha}\frac{1}{1+2\xi\vert\tilde{H}\vert^{2}}\Big(\vert\dot{\tilde{H}}\vert^{2}+a^{-2}\vert\nabla\tilde{H}\vert^{2}\Big)\\
+a^{-2\alpha}4\xi^{2}\frac{\vert\tilde{H}\vert^{4}}{(1+2\xi\vert\tilde{H}\vert^{2})^{2}}.\label{eq:nonminimalenergyprogramunits}
\end{multline}


\subsection{Friedmann equations}

Varying the action (\ref{eq:action_einstein_frame}) with respect
to the metric (recall: Palatini formulation, the Ricci tensor is independent
of the metric), we obtain:

\begin{multline}
0=\int d^{4}x\sqrt{-g}\delta g^{\mu\nu}\Bigg(2M_{P}^{2}\big(R_{\mu\nu}-\frac{1}{2}g_{\mu\nu}R\big)\\
+\frac{1}{1+2\xi\vert H\vert^{2}/M_{P}^{2}}\Big(\partial_{\mu}H^{*}\partial_{\nu}H-\frac{1}{2}g_{\mu\nu}g^{\rho\sigma}\partial_{\rho}H^{*}\partial_{\sigma}H\Big)-\frac{\lambda g_{\mu\nu}\vert H\vert^{4}}{\left(1+2\xi\vert H\vert^{2}/M_{P}^{2}\right)^{2}}\Bigg)\label{eq:nonminimalactionvariedmetric}
\end{multline}
A first equation can be obtained from working on the 00 component
of (\ref{eq:nonminimalactionvariedmetric}):

\begin{equation}
(3+6\alpha)M_{P}^{2}\mathbb{\mathcal{H}}^{2}=\frac{1}{4+8\xi\vert H\vert^{2}/M_{P}^{2}}\Big(\vert\dot{H}\vert^{2}+a^{-2+2\alpha}\vert\nabla H\vert^{2}\Big)+\frac{1}{2}\frac{a^{2\alpha}\lambda\vert H\vert^{4}}{\left(1+2\xi\vert H\vert^{2}/M_{P}^{2}\right)^{2}}.\label{eq:friedmannh2}
\end{equation}
And another one can be read off the $ii$ component (sum on $i$):
\begin{multline}
M_{P}^{2}\big(R_{ii}-\frac{1}{2}g_{ii}R\big)+\frac{1}{2+4\xi\vert H\vert^{2}/M_{P}^{2}}\Big(\partial_{i}H^{*}\partial_{i}H+\frac{1}{2}g_{ii}\big(a^{-2\alpha}\vert\dot{H}\vert^{2}-a^{-2}\vert\nabla H\vert^{2}\big)\Big)\\
-\frac{1}{2}g_{ii}\frac{\lambda\vert H\vert^{4}}{\left(1+2\xi\vert H\vert^{2}/M_{P}^{2}\right)^{2}}=0,
\end{multline}
yielding the following after some cleanup:
\begin{multline}
\ddot{a}=-\frac{1}{2}a(1-2\alpha)\mathcal{H}^{2}-\frac{1}{12M_{P}^{2}}\frac{a}{1+2\xi\vert H\vert^{2}/M_{P}^{2}}\Big(\frac{3}{2}\vert\dot{H}\vert^{2}-\frac{1}{2}a^{-2+2\alpha}\vert\nabla H\vert^{2}\Big)\\
+\frac{1}{4M_{P}^{2}}a^{1+2\alpha}\frac{\lambda\vert H\vert^{4}}{\left(1+2\xi\vert H\vert^{2}/M_{P}^{2}\right)^{2}}.
\end{multline}
We thus have two equations of motion for the scale factor, which can
be used to check consistency when evolving the problem on the lattice.
Upon rescaling with (\ref{eq:nonunitaryglobalrescaling}) they read

\begin{align}
(3+6\alpha)\mathbb{\mathcal{H}}^{2} & =\frac{1}{4+8\xi\vert\tilde{H}\vert^{2}}\Big(\vert\dot{\tilde{H}}\vert^{2}+a^{-2+4\alpha}\vert\nabla\tilde{H}\vert^{2}\Big)+2\xi^{2}a^{4\alpha}\frac{\vert\tilde{H}\vert^{4}}{\left(1+2\xi\vert\tilde{H}\vert^{2}\right)^{2}}\label{eq:friedmannh2rescaled}
\end{align}
and
\begin{equation}
\ddot{a}=-\frac{a}{2}(1-2\alpha)\mathcal{H}^{2}-\frac{a}{24+48\xi\vert\tilde{H}\vert^{2}}\Big(3\vert\dot{\tilde{H}}\vert^{2}-a^{-2+4\alpha}\vert\nabla\tilde{H}\vert^{2}\Big)+\frac{a^{1+4\alpha}\xi^{2}\vert\tilde{H}\vert^{4}}{\left(1+2\xi\vert\tilde{H}\vert^{2}\right)^{2}}.\label{eq:friedmanaddotnonminimalprogramunits}
\end{equation}


\subsection{Equation of motion of the Higgs}

Since we chose to use a complex field to emulate the Higgs field,
we may use several representations. We can obtain the equation of
motion for the complex $H$ itself, or we can decompose it to $H=\phi_{1}+i\phi_{2}$
and calculate the dynamics of $\phi_{1}$ or $\phi_{2}$, or lastly
parametrize using the norm and the phase: $H=\rho e^{i\theta}.$

\subsubsection{Using $H$ and $H^{*}$}

To obtain the equation of motion of $H$, we vary the action in $H^{*}$:
$H^{*}\to H^{*}+\delta H^{*}$. The following is obtained:
\begin{multline}
\ddot{H}=a^{-2(1-\alpha)}\nabla^{2}H-(3-\alpha)\dot{H}\mathcal{H}\\
+\frac{1}{M_{P}^{2}}\frac{2\xi H^{*}}{1+2\xi\vert H\vert^{2}/M_{P}^{2}}\big(\dot{H}^{2}-a^{-2(1-\alpha)}(\nabla H)^{2}\big)\\
-\frac{2a^{2\text{\ensuremath{\alpha}}}\lambda\vert H\vert^{2}H}{(1+2\xi\vert H\vert^{2}/M_{P}^{2})^{2}}
\end{multline}
Upon rescaling as defined in equation (\ref{eq:nonunitaryglobalrescaling}):
\begin{multline}
\ddot{\tilde{H}}=a^{-2+4\alpha}\nabla^{2}\tilde{H}-(3-\alpha)\dot{\tilde{H}}\mathcal{H}\\
+\frac{2\xi\tilde{H}^{*}}{1+2\xi\vert\tilde{H}\vert^{2}}\big(\dot{\tilde{H}}^{2}-a^{-2+4\alpha}(\nabla\tilde{H})^{2}\big)-\frac{8\xi^{2}a^{4\text{\ensuremath{\alpha}}}\vert\tilde{H}\vert^{2}\tilde{H}}{(1+2\xi\vert\tilde{H}\vert^{2})^{2}}\label{eq:eomcomplexhiggs}
\end{multline}
This equation, if correct, must be the same as the one we obtained
in unitary gauge. To verify this, fix the unitary gauge in (\ref{eq:eomcomplexhiggs}).
We obtain the following:
\begin{equation}
\ddot{\tilde{h}}=a^{-2(1-2\alpha)}\nabla^{2}\tilde{h}-(3-\alpha)\dot{\tilde{h}}\mathcal{H}+\frac{\xi\tilde{h}}{1+\xi\tilde{h}^{2}}\big(\dot{\tilde{h}}^{2}-a^{-2(1-2\alpha)}(\nabla\tilde{h})^{2}\big)-\frac{4\xi^{2}a^{4\text{\ensuremath{\alpha}}}\tilde{h}^{3}}{(1+\xi\tilde{h}^{2})^{2}}\label{eq:eomHtoUnitary}
\end{equation}
When inserting the usual field redefinition (\ref{eq:h_of_chi}) accounting
for (\ref{eq:unitaryglobalrescaling}) and (\ref{eq:nonunitaryglobalrescaling}):
$\tilde{h}=\frac{1}{\sqrt{\xi}}\sinh(\sqrt{\xi}\tilde{\chi})$, we
obtain
\begin{equation}
\ddot{\tilde{\chi}}-a^{-2(1-2\alpha)}\nabla^{2}\tilde{\chi}+(3-\alpha)\dot{\tilde{\chi}}\mathcal{H}+4\sqrt{\xi}a^{4\alpha}\frac{\tanh(\sqrt{\xi}\tilde{\chi})^{3}}{\cosh(\sqrt{\xi}\tilde{\chi})^{2}}=0
\end{equation}
which is indeed equation (\ref{eq:chiunitaryeomrescaled}). 

\subsubsection{Using $H=\frac{1}{\sqrt{2}}\big(\phi_{1}+i\phi_{2}\big)$}

This parametrization is less convenient because we have to treat the
two degrees of freedom separately. It is however the closest to what
the equations of a full $SU(2)$ doublet will look like. The two equations
for $\phi_{1}$ and $\phi_{2}$ (obtained by varying the action with
respect to the corresponding field) are as follows:

\begin{multline}
\ddot{\phi}_{1}=a^{-2+2\alpha}\nabla^{2}\phi_{1}-\frac{a^{-2+2\alpha}\xi}{1+\xi\vert H\vert^{2}}\Big(2\phi_{2}\nabla\phi_{1}\nabla\phi_{2}+\phi_{1}(\nabla\phi_{1})^{2}-\phi_{1}(\nabla\phi_{2})^{2}\Big)\\
-(3-\alpha)\dot{\phi}_{1}\mathcal{H}+\frac{\xi}{1+\xi\vert H\vert^{2}}\Big(2\phi_{2}\dot{\phi}_{1}\dot{\phi}_{2}+\phi_{1}(\dot{\phi}_{1}^{2}-\dot{\phi}_{2}^{2})\Big)-\frac{a^{2\alpha}\lambda\vert H\vert^{2}\phi_{1}}{\big(1+\xi\vert H\vert^{2}\big)^{2}}\label{eq:nonminimaleomphi1}
\end{multline}

\begin{multline}
\ddot{\phi}_{2}=a^{-2+2\alpha}\nabla^{2}\phi_{2}-\frac{a^{-2+2\alpha}\xi}{1+\xi\vert H\vert^{2}}\Big(2\phi_{1}\nabla\phi_{1}\nabla\phi_{2}-\phi_{2}(\nabla\phi_{1})^{2}-\phi_{2}(\nabla\phi_{2})^{2}\Big)\\
-(3-\alpha)\dot{\phi}_{2}\mathcal{H}+\frac{\xi}{1+\xi\vert H\vert^{2}}\Big(2\phi_{1}\dot{\phi}_{1}\dot{\phi}_{2}-\phi_{2}(\dot{\phi}_{1}^{2}-\dot{\phi}_{2}^{2})\Big)-\frac{a^{2\alpha}\lambda\vert H\vert^{2}\phi_{2}}{\big(1+\xi\vert H\vert^{2}\big)^{2}}\label{eq:nonminimaleomphi2}
\end{multline}
Note that the fields are here expressed as a factor of the Planck
mass so that the equations take up less space. We can always add the
Planck masses back by dimensional analysis. As done with the $H/H^{*}$
representation, we can fix the gauge by setting for example $\phi_{2}$
to zero.

\begin{equation}
\ddot{\phi}_{1}=a^{-2(1-\alpha)}\nabla^{2}\phi_{1}-3\mathcal{H}\dot{\phi}_{1}+\frac{\xi\phi_{1}}{1+\xi\phi_{1}^{2}}\big(\dot{\phi}_{1}^{2}-a^{-2(1-\alpha)}(\nabla\phi_{1})^{2}\big)-\frac{\lambda a^{2\alpha}\phi_{1}^{3}}{\big(1+\xi\phi_{1}^{2}\big)^{2}}\label{eq:unitaryfromphi1phi2}
\end{equation}
which indeed corresponds to (\ref{eq:eomHtoUnitary}) once the rescaling
(\ref{eq:nonunitaryglobalrescaling}) is applied. 

\subsubsection{Using $H=\rho e^{i\theta}$}

\label{subsec:normthetaparam}By $\rho$ we do not actually mean the
norm of the complex number, but the value of the unitary gauge version
of the Higgs $h$. The idea is to decouple the oscillation of the
homogeneous component from the eventual phase perturbations. $\theta$
then only encodes the distribution between real and imaginary components:
\[
H=\vert\rho\vert e^{i\pi\Theta[-\rho]}e^{i\theta}
\]
with $\Theta$ the Heaviside distribution. This way of doing thing
is not ideal for a lattice study because of the singularity of the
coordinates at $\rho=0$, but we will use it for a linearized analysis. 

Varying with respect to $\rho$ is not very instructive as it yields
yet another version of equation (\ref{eq:eomHtoUnitary}) once $\theta$
is set to zero. $\theta$ contains the more interesting information
however, because it encodes the difference with the unitary gauge
case only. The variation of the action in $\theta$ yields:

\begin{equation}
\ddot{\theta}=a^{-2(1-\alpha)}\nabla^{2}\theta-(3-\alpha)\mathcal{H}\dot{\theta}-\frac{2}{\rho+2\xi\rho^{3}/M_{P}^{2}}\Big(\dot{\rho}\dot{\theta}-a^{-2(1-\alpha)}\nabla\rho\nabla\theta\Big)\label{eq:nonminimaleomphase}
\end{equation}
We see that the presence of the gradient terms can allow $\text{\ensuremath{\theta}}$
to evolve even if it is initially zero. At the exception of the Planck
mass disappearing this equation is untouched under the rescaling (\ref{eq:unitaryglobalrescaling})
because all the terms contain the same number of derivatives. We write
it again for convenience:
\begin{equation}
\ddot{\theta}=a^{-2(1-\alpha)}\nabla^{2}\theta-(3-\alpha)\mathcal{\tilde{H}}\tilde{\dot{\theta}}-\frac{2}{\tilde{\rho}+2\xi\tilde{\rho}^{3}}\Big(\dot{\tilde{\rho}}\tilde{\dot{\theta}}-a^{-2(1-\alpha)}\nabla\tilde{\rho}\nabla\theta\Big)\label{nonminimaleomphaserescaled}
\end{equation}
where $\dot{\theta}$ transforms under the rescaling, but not $\theta$. 

\subsection{Slow roll initial condition}

We need an initial condition for the fields at the beginning of preheating,
that is shortly before the inflaton exits its slow roll epoch. During
slow roll, the spatial fluctuations in the inflaton field $\tilde{H}$
are negligible, $\ddot{\tilde{H}}$ is zero and $\dot{\tilde{H}}$
is small. Starting from equation (\ref{eq:eomcomplexhiggs}), we neglect
the spatial fluctuations. We are left with a second order polynomial
to solve to get the slow-roll value of $\dot{\tilde{H}}:$

\begin{align}
\frac{2\xi\tilde{H}}{1+2\xi\vert\tilde{H}\vert^{2}}\dot{\tilde{H}}^{2}-(3-\alpha)\mathcal{H}\dot{\tilde{H}}-\frac{8\xi^{2}a^{4\text{\ensuremath{\alpha}}}\vert\tilde{H}\vert^{2}\tilde{H}}{(1+2\xi\vert\tilde{H}\vert^{2})^{2}} & =0\label{eq:slowroll}
\end{align}
The initial Hubble parameter can be obained under the same assumptions
from equation (\ref{eq:friedmannh2rescaled}):

\begin{equation}
\mathbb{\mathcal{H}}^{2}\Big\vert_{\mathrm{slow\,roll}}=\frac{2\xi^{2}a^{4\alpha}}{3+6\alpha}\frac{\vert\tilde{H}\vert^{4}}{\left(1+2\xi\vert\tilde{H}\vert^{2}\right)^{2}}\label{eq:friedmannh2-slowroll}
\end{equation}


\subsection{Initial fluctuations}

The quantum fluctuations in the fields at the end of inflation should
not be different from those of the unitary gauge, because the field
transformation (\ref{eq:h_of_chi}) that relates $\chi$ and $h$
leaves the perturbations untouched at linear order. Then $H$ actually
has two components: these were treated as two scalar fields, each
receiving fluctuations on top of its homogeneous component separately.

\section{Low storage Runge-Kutta scheme}

\label{sec:Low-storage-Runge-Kutta}The Velocity Verlet and Leapfrog
schemes already implemented in \cosmolattice$\,$ are not well suited
for dynamical equations that contain friction terms. To evolve our
new system with a nonminimal Higgs we thus need another explicit scheme.
The $2N$-storage types of Runge-Kutta methods are especially adapted
because of their low temporary storage requirements and robustness
against friction terms. Algorithm 2 in its first and third order flavors
from~\cite{bazavov2021} was added as a new integrator class to \cosmolattice,
and tested against the already in place Velocity Verlet algorithms.
Small scale simulations of Palatini preheating with scalar bosons
with the Velocity Verlet 2 (VV2) and Runge Kutta 3 (RK3) integrators
were ran as an example. Some trajectories from this comparison are
shown in figure \ref{fig:comparisonrk3vv2}.\fancytab[\centering]{vv2vsrk3}%
{Integrator comparison}%
{% 	
\begin{tabular}{rlrl}  	
 \multicolumn{4}{c}{Model: $\texttt{higgs\_palatini\_gauge\_scalars}$}  \\
 \midrule 	
 $N$   &    64 &     $\tilde{\Delta t}$ & $10^{-5}$ \\
 $\tilde{k}_\mathrm{IR}$ & 460 & $\tilde{k}_\mathrm{cut-off}$ & 3000 \\
 $\chi(0)$ & $3.05\cdot 10^{15}$ & $\dot{\chi}(0)$ & $-6.37 \cdot 10^{26} $ \\
 evolver & VV2 / RK3 & $\tilde{t}_\mathrm{max}$ & 0.05 \\
 \midrule  	
core hours & 0.20 / 0.48 & rel. energy conservation & $3.0\cdot10^{-3}$ / $2.7\cdot10^{-3}$\\ 
\end{tabular}%  
}{ht!}

Tests showed that the order of convergence is indeed 3 for the RK3
algorithm, but as one can see from the last row of box \ref{simulation:vv2vsrk3}
the performance is not as good as that of the second order Velocity
Verlet. This is due to the fact that an extra copy of all the fields
has to be written to the memory at every step, unlike the Verlet scheme
that has a leapfrog structure and does not need to carry a copy of
the system. It is thus probably only desirable to use the new Runge
Kutta algorithm for cases with friction. 
\begin{figure}[h]
\centering
\input{figures/comparison_vv2_rk3.pgf}
\vspace{-0.4cm}

\caption{Some output from the two simulations \ref{simulation:vv2vsrk3}, comparing
the existing Velocity Verlet integrator with the new low storage Runge
Kutta. \label{fig:comparisonrk3vv2}}
\end{figure}


\section{Numerical instability in the non-minimal dynamical equations}

Equations (\ref{eq:eomcomplexhiggs}), (\ref{eq:nonminimaleomphi1}),
(\ref{eq:nonminimaleomphi2}) and (\ref{eq:nonminimaleomphase}) are
all different representations of the same dynamics and thereby share
similar features. When going to unitary gauge, these features are
still present (see equation \ref{eq:eomHtoUnitary}): namely a cancellation
between a second derivative and a squared derivative term. Let us
provide a minimal example by simplifying equation (\ref{eq:eomHtoUnitary}):
we neglect the expansion of the Universe and set $\alpha\to0$. 
\begin{equation}
\Bigg(\underbrace{\ddot{h}-\frac{\xi h}{1+\xi h^{2}}(\dot{h})^{2}}_{\#1}\Bigg)-\Bigg(\underbrace{\nabla^{2}h-\frac{\xi h}{1+\xi h^{2}}(\nabla h)^{2}}_{\#2}\Bigg)+\frac{4\xi^{2}h^{3}}{(1+\xi h^{2})^{2}}=0\label{eq:simplified_h_eom_nonminimal}
\end{equation}
The highlighted terms {\small{}$\#1$} and {\small{}$\#2$ }above
have the same structure, the usual field definition (\ref{eq:h_of_chi})
simplifies them according to

\[
h''-\frac{\xi h}{1+\xi h^{2}}(h')^{2}\xrightarrow[\text{}]{\text{(\ref{eq:h_of_chi})}}\chi''.
\]
This yields the numerically stable equation (\ref{eq:chiunitaryeom}).
In other words, (\ref{eq:simplified_h_eom_nonminimal}) is analytically
equivalent to a frictionless system. The numerical instability appears
on the lattice: $\ddot{h}$ and $\dot{h}^{2}$ are local terms, whereas
$\nabla^{2}h$ and $(\nabla h)^{2}$ are not. Their cancellation on
a lattice is thereby not as straightfoward. When the perturbations
start to grow, any deviation in (\ref{eq:simplified_h_eom_nonminimal}
{\small{}$\#2$}) will stack with each timestep until the system blows
up. As expected however, the equation is stable in the absence of
perturbations -- that is when (\ref{eq:simplified_h_eom_nonminimal}
{\small{}$\#2$}) trivially cancels:

\begin{equation}
\ddot{h}-\frac{\xi h}{1+\xi h^{2}}(\dot{h})^{2}+\frac{4\xi^{2}h^{3}}{(1+\xi h^{2})^{2}}=0\label{eq:simplifiedheomnonminimalhomogeneous}
\end{equation}
This translates to lattice simulations of equations (\ref{eq:eomcomplexhiggs})
or (\ref{eq:simplified_h_eom_nonminimal}) that are stable right after
the exit of inflation (when the energy stored in the gradients is
small), and become unstable in a few timesteps at the onset of the
perturbations. An example is shown in figure \ref{fig:nonminimalscalarhiggsinstability},
which displays the lattice evolution of (\ref{eq:eomcomplexhiggs})
together with the Friedmann equation (\ref{eq:friedmanaddotnonminimalprogramunits}).
\begin{figure}
\centering
\input{figures/h_nonminimalcoupling_instability.pgf}
\vspace{-0.4cm}

\caption{Lattice run of equation (\ref{eq:eomcomplexhiggs}). The numerical
instability shows as soon as the gradient terms become relevant. (\textit{Left})
Evolution of the field average and its homogeneous version. (\textit{Right})
Fraction of the energy contained in the gradients of the field. The
energy expression is taken from equation (\ref{eq:nonminimalenergyprogramunits})
in unitary gauge. Notice the familiar shape of the growth of the gradient
energy (compare with figure \ref{fig:scalarpalatinifieldandenergies}).
The units are in term of (\ref{eq:nonunitaryglobalrescaling}). \label{fig:nonminimalscalarhiggsinstability}}
\end{figure}
This instability is problematic: friction terms like those of equation
(\ref{eq:simplified_h_eom_nonminimal}) are a common feature of theories
with non-canonical kinetic terms~\cite{Gwyn2013}. Non-canonical kinetic
terms are expected to be present in any theory with multi-component
fields and a non-minimal coupling to gravity, which is why it is worth
trying to heal the instability. Another workaround would be to obtain
the equations of motion in the Jordan frame. However, this would be
more technically challenging in the Palatini case because the variation
would have to be taken with respect to the degrees of freedom of the
connection (in the Ricci tensor) as well. Since one would start from
an action free of non-canonical kinetic terms however, there is hope
that no friction terms would appear in the field part of the dynamical
equations. In the context of this project, we focused on healing the
numerical instability, but the Jordan frame approach should not be
neglected in the future.


\subsection{1D instability}

\label{subsec:1dcode}A minimal 1D lattice code was written in Python,
only to confirm that the numerical instability of equation (\ref{eq:simplified_h_eom_nonminimal})
survives in one dimension. The gradient and Laplacian operators were
implemented at up to order 8, but the instability persisted even with
very thin space and time discretizations. In the following subsections
we present some of the strategies we tried to tame the numerical instability.

\subsubsection{Naive implicit finite differences scheme}

Implicit schemes have the reputation of being a lot more stable than
their explicit counterparts. Thereby a centered second order implicit
scheme was given a try, yielding the following discretized version
of (\ref{eq:simplified_h_eom_nonminimal}):
\begin{multline}
\Bigg(\frac{h_{i-1,j}-2h_{i,j}+h_{i+1,j}}{\Delta t^{2}}-\frac{h_{i+1,j}}{1+\xi h_{i+1,j}^{2}}\frac{(h_{i+1,j}-h_{i-1,j})^{2}}{4\Delta t^{2}}\Bigg)\\
-\Bigg(\frac{h_{i,j-1}-2h_{i,j}+h_{i,j-1}}{\Delta x^{2}}-\frac{h_{i+1,j}}{1+\xi h_{i+1,j}^{2}}\frac{(h_{i,j+1}-h_{i,j-1})^{2}}{4\Delta x^{2}}\Bigg)\\
+4\frac{\xi^{3}h_{i+1,j}^{3}}{(1+\xi h_{i+1,j}^{2})^{2}}=0
\end{multline}
with $h_{i,j}\coloneqq h(i\Delta t,j\Delta x)$ and periodic boundary
conditions in the $x$ direction. This equation was numerically solved
for $h_{i+1,j}$ at each timestep but the instability persisted and
appeared at the same time as with an explicit scheme. So the instability
persists in a simplified system, in one dimension and with an implicit
scheme. In the next section, we will try to cast the system as an
advection problem to be able to fallback on the stability conditions
of such problems.

\subsubsection{Casting as an advection problem}

Rather than solving only for the field $h$, let us solve and keep
track of three different fields instead. Namely, define:
\begin{equation}
\Pi\coloneqq\dot{h}\quad\quad D\coloneqq\nabla h\quad\quad h\coloneqq h.\label{eq:def_auxiliary_fields_unstableproblem}
\end{equation}
Allowing the gradient to be its own degree of freedom allows for the
problem to be cast as an ensemble of first order equations, rather
than a single second order one. In terms of the definitions (\ref{eq:def_auxiliary_fields_unstableproblem}),
equation (\ref{eq:simplified_h_eom_nonminimal}) is written 
\begin{equation}
\dot{\Pi}-\nabla D=f(h)\Big(\Pi^{2}-D^{2}-4hf(h)\Big)\coloneqq J(h,\Pi,D),
\end{equation}
where $f(h)=\xi h/\big(1+\xi h^{2}\big)$. This gives us the drift
of the field $\Pi$, but we also need the drifts of $h$ and $D$.
The former is easy because it is given by its definition (\ref{eq:def_auxiliary_fields_unstableproblem}).
The latter is given by a trick. Remember than $\partial_{t}$ and
$\nabla$ commute:
\begin{equation}
\partial_{t}D=\partial_{t}\nabla h=\nabla\partial_{t}h=\nabla\Pi,
\end{equation}
which gives the drift of $D$. The system to evolve is then
\begin{equation}
\begin{cases}
\Pi & =\dot{h},\\
\dot{D} & =\nabla\Pi,\\
\dot{\Pi} & =\nabla D+J(h,\Pi,D).
\end{cases}
\end{equation}
We should be able to cast this as a transport equation if we want
to fall back on the stability condition of this class of problems.
In particular if we work again in 1D, we should be able to find $A$
and $\vec{\Phi}$ such that
\begin{equation}
\partial_{t}\vec{\Phi}+A\,\partial_{x}\vec{\Phi}=\vec{J}(\vec{\Phi}).\label{eq:advectionform}
\end{equation}
In our case, we have
\begin{equation}
\left(\begin{array}{c}
\dot{h}\\
\dot{\Pi}\\
\dot{D}
\end{array}\right)+\underbrace{\left(\begin{array}{ccc}
0 & 0 & 0\\
0 & 0 & -1\\
0 & -1 & 0
\end{array}\right)}_{A}\left(\begin{array}{c}
\nabla h\\
\nabla\Pi\\
\nabla D
\end{array}\right)=\left(\begin{array}{c}
\dot{h}\\
\dot{\Pi}-\nabla D\\
\dot{D}-\nabla\Pi
\end{array}\right)=\underbrace{\left(\begin{array}{c}
\Pi\\
J(h,\Pi,D)\\
0
\end{array}\right)}_{\vec{J}}.
\end{equation}
Traditionally, transport equations are given for a single field, so
the fact that the fields are mixed by the matrix $A$ here stands
in the way. However, we can make the system become the sum of 3 separate
transport equations with a field redefinition. Namely, we just need
to diagonalize $A$:
\begin{equation}
\Lambda=P^{-1}AP
\end{equation}
with $\Lambda=\mathrm{diag}(\lambda_{1},\lambda_{2},\lambda_{3})$.
Equation (\ref{eq:advectionform}) can now be transformed:
\begin{align}
P^{-1}\partial_{t}\vec{\Phi}+P^{-1}AP\,P^{-1}\,\partial_{x}\vec{\Phi} & =P^{-1}\vec{J}(\vec{\Phi})\nonumber \\
P^{-1}\partial_{t}\vec{\Phi}+\Lambda P^{-1}\,\partial_{x}\vec{\Phi} & =P^{-1}\vec{J}(\vec{\Phi}).
\end{align}
Where the matrix $P$ containing the eigenvectors of $A$ directly
gives the field redefinition that makes $A$ diagonal. Explicitely
we have three equations corresponding to the three eigenvalues of
$A$:

\begin{equation}
\begin{cases}
\lambda_{1}=-1 & \dot{U}_{1}-\nabla U_{1}=J(h,\Pi,D)\\
\lambda_{2}=1 & \dot{U}_{2}+\nabla U_{2}=-J(h,\Pi,D)\\
\lambda_{3}=0 & \dot{h}=\Pi
\end{cases}\label{eq:diagonalizedeqnonminimalhiggs}
\end{equation}
Where we defined $U_{1}=D+\Pi$ and $U_{2}=D-\Pi$. The last equation
has $\lambda=0$ and is therefore not a transport equation, so its
discretization should not pose a problem. The other two are numerically
stable if an upwind scheme is chosen for the gradients~\cite{Courant1967}.
That is, we choose a backward finite difference if $\lambda_{i}$
is positive, and a forward finite difference otherwise. With this
in mind, (\ref{eq:diagonalizedeqnonminimalhiggs}) at first order
becomes----
\begin{align}
\big(U_{1}\big)_{i+1,j}-\big(U_{1}\big)_{i,j} & =\frac{\Delta t}{\Delta x}\bigg(\big(U_{1}\big)_{i,j+1}-\big(U_{1}\big)_{i,j}\bigg)+\Delta tJ(h_{i,j},\Pi_{i,j},D_{i,j}),\\
-\big(U_{2}\big)_{i+1,j}+\big(U_{2}\big)_{i,j} & =\frac{\Delta t}{\Delta x}\bigg(\big(U_{2}\big)_{i,j}-\big(U_{2}\big)_{i,j-1}\bigg)+\Delta tJ(h_{i,j},\Pi_{i,j},D_{i,j}),\\
h_{i+1,j}-h_{i,j} & =\Delta t\Pi_{i,j}=\frac{\Delta t}{2}\bigg(\big(U_{1}\big)_{i,j}-\big(U_{2}\big)_{i,j}\bigg).
\end{align}

Now it turns out that using an upwind scheme can be thought of as
artificially adding some diffusion on the right hand side of equations
(\ref{eq:advectionform})~\cite{Linge2017}. We can thereby also think
of adding such a term for extra damping of the perturbations:
\begin{equation}
\partial_{t}\vec{\Phi}+A\,\partial_{x}\vec{\Phi}=\vec{J}(\vec{\Phi})+\eta\nabla^{2}\vec{\Phi}
\end{equation}
which under the diagonalization introduced before becomes

\begin{equation}
\begin{cases}
\lambda_{1}=-1 & \dot{U}_{1}-\nabla U_{1}=J(h,\Pi,D)+\eta\nabla^{2}U_{1}\\
\lambda_{2}=1 & \dot{U}_{2}+\nabla U_{2}=-J(h,\Pi,D)-\eta\nabla^{2}U_{2}\\
\lambda_{3}=0 & \dot{h}=\Pi+\eta\nabla^{2}h
\end{cases}\label{eq:diagonalizedeqnonminimalhiggswithvisco}
\end{equation}
Unfortunately, the instability persisted even when evolving the $h$,
$U_{1}$ and $U_{2}$ fields with the correct finite difference. Adding
more viscosity artificially as in equation (\ref{eq:diagonalizedeqnonminimalhiggswithvisco})
did stabilize the evolution, at the cost of preventing the growth
of or even damping completely the initial perturbations. A C++ implementation
of (\ref{eq:diagonalizedeqnonminimalhiggswithvisco}) is made available
in a GitHub repository.\footnote{\url{https://github.com/duxfrederic/nonminimal-higgs-1D-cpp}}

\begin{figure}[H]
\centering
\input{figures/1D_phase_evolution_heatmap.pgf}
\vspace{-1cm}

\caption{Evolution of the ``norm'' (see section \ref{subsec:normthetaparam})
and phase of a complex Higgs on a 1D lattice. We see the tachyonic
mode building up fast at the end of the evolution on top of $\tilde{\rho}$.
The phase is unaffected and simply oscillates. The units are in terms
of (\ref{eq:nonunitaryglobalrescaling}). \label{fig:1Devolutionofphasedof}}
\end{figure}


\section{Linearized analysis of the phase evolution}

\label{sec:Linearizedanalysisphase}Even if a fully fledged lattice
analysis of the phase degree of freedom $\theta$ is technically challenging,
we can still study the early evolution by linearizing the equations.
The problem is that this approach will not work when the norm becomes
dominated by its gradient energy, which is the regime where the phase
might become excited as well.

Nevertheless, to gain some insight into the early evolution, let us
expand (\ref{nonminimaleomphaserescaled}) as follows
\begin{equation}
\tilde{\rho}(\tilde{x},\tilde{t})\to\tilde{\rho}(\tilde{t})+\delta\tilde{\rho}(\tilde{t})e^{i\tilde{k}_{t}\tilde{x}}\,\,\,\,\,\,\theta(\tilde{x},\tilde{t})\to\theta(\tilde{t})+\delta\theta(\tilde{t})e^{i\tilde{k}\tilde{x}}
\end{equation}
where $\tilde{k}_{t}\approx500$ is the typical momentum at which
the tachyonic perturbations in the norm grow and $\tilde{k}$ is some
momentum in the phase. The first order gives

\begin{equation}
\ddot{\delta\theta}(\tilde{t})+2\frac{\dot{\delta\theta}(\tilde{t})\dot{\tilde{\rho}}(\tilde{t})+\dot{\delta\tilde{\rho}}(\tilde{t})\dot{\theta}(\tilde{t})}{\tilde{\rho}(\tilde{t})+2\xi\tilde{\rho}^{3}(\tilde{t})}+(3-\alpha)\mathcal{\tilde{H}}\tilde{\dot{\delta\theta}}(\tilde{t})+a^{-2+2\alpha}\tilde{k}^{2}\delta\theta(\tilde{t})=0
\end{equation}
which luckily does not depend on $\tilde{k}_{t}$. Without the Hubble
friction term this is just an oscillator whose frequency depends on
$k$. Thus there is no value of $\tilde{k}$ that can make the amplitude
of the perturbations $\delta\theta(\tilde{t})$ grow. Moreover, in
the presence of Hubble friction any initial perturbation will flatten
out with time. We also evolved equation (\ref{eq:nonminimaleomphase})
and its norm counterpart numerically on a simple 1D lattice code.
The result is presented in figure \ref{fig:1Devolutionofphasedof}
which offers a visualization of how different the norm and phase degrees
of freedom are in regard of the creation of perturbations. We also
see the position space version of how the tachyonic mechanism only
operates in a narrow range of wavelengths, resulting in a large scale
standing wave. This combined linearized and simplified lattice analysis
seems to tell us that the phase degree of freedom does not help in
the process of preheating. The power spectrum of the fluctuations
of $\theta(\tilde{x},\tilde{t})$ of figure \ref{fig:1Devolutionofphasedof}
is not evolving with time, even as $\tilde{\rho}(\tilde{x},\tilde{t})$
becomes very structured at $\tilde{t}\gtrsim0.020$. 

\section{Lattice analysis of the early phase evolution}

The analysis of section \ref{sec:Linearizedanalysisphase} used a
polar representation of the complex $\tilde{H}$, but we can also
produce a lattice analysis using a \cosmolattice$\,$ complex field.
Because of the numerical instability at play in all the complex Higgs
equations, we can only simulate the early evolution -- when the perturbations
are small. Thus the regime of validity of the simulation is the same
as that of the linear analysis of the last section. Nevertheless,
we put to test the result of the linear analysis, namely that the
phase degree of freedom does not exponentially grow perturbations.
To do so, we start the simulation with the homogeneous component of
the inflaton set entirely in the real part. The initial homogeneous
value of the phase is then zero. The summary of the simulation is
given in box \ref{simulation:complexhiggslinearregime} and the resulting
trajectories are shown in figure \ref{fig:lattice_complex_higgs}.
We see that the lattice evolution of the Higgs closely matches that
of the homogeneous counterpart, meaning that the perturbations are
indeed small enough to not cause a significant backreaction on the
zero-mode. While the gradient energy follows the usual pattern of
intermittent exponential growth, the standard deviation accross the
lattice of the phase remains approximately constant. The peaks appearing
whenever $\langle\vert\tilde{H}\vert\rangle$ crosses zero are due
to the coordinate singularity in the polar coordinates and are therefore
not physical. We might also want to justify why the second peak is
broader: it is simply because at that second zero-crossing, the field
$\tilde{H}$ is already more dispersed around its average because
of the perturbations. The crossing-time is then longer causing the
coordinate singularity to be visible for longer as well. We stress
again that this simulation only covers the regime of weak perturbations,
and does predict whether the final phase of thermalization of the
tachyonic excitations-dominated Universe can be accelerated by the
availability of this extra degree of freedom. \fancytab[\centering]{complexhiggslinearregime}%
{Complex Higgs on the lattice in the linear regime}%
{%
\begin{tabular}{rlrl}  	
 \multicolumn{4}{c}{Model: $\texttt{complex\_higgs\_palatini}$}  \\
 \midrule 	
 $N$   &    128 &     $\tilde{\Delta t}$ & $5 \cdot 10^{-6}$ \\
 $\tilde{k}_\mathrm{IR}$ & 200 & $\tilde{k}_\mathrm{cut-off}$ & 1000 \\
 $\tilde{H}(0)$ & $3.05\cdot 10^{15} + 0 i$ & $\tilde{H}(0)$ & $-6.37 \cdot 10^{26} $ \\
 evolver & Runge Kutta 3$^\mathrm{rd}$ order & $\tilde{t}_\mathrm{max}$ & 0.03 \\
 \midrule  	
core hours & 2  & relative energy conservation & $6\cdot10^{-3}$\\ 
\end{tabular}%  
}{}
\begin{figure}[H]
\centering
\input{figures/lattice_complex_higgs.pgf}
\vspace{-0.7cm}

\caption{Lattice evolution of equation (\ref{eq:eomcomplexhiggs}) with the
self-consistent expansion (\ref{eq:friedmanaddotnonminimalprogramunits}).
(\textit{Top}) Average of the real part of the field. The homogeneous
part of the imaginary part was initially set to zero. (\textit{Middle})
Growth of the gradient energy content due to the usual tachyonic excitations
in the norm. (\textit{Bottom}) observed standard deviation in the
argument $\theta=\arctan(\mathrm{Im}\tilde{H}/\mathrm{Re}\tilde{H})$.
The units are in terms of (\ref{eq:nonunitaryglobalrescaling}). \label{fig:lattice_complex_higgs}}
\end{figure}

