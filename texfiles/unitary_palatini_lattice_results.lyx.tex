
\section{Palatini Higgs preheating on the lattice}

\label{sec:Palatini-Higgs-preheating-lattice}After validating our
methodology with the new code \cosmolattice$\,$, we now turn to the
subject of study of this project. The units of the simulations will
now be given by the transformation (\ref{eq:unitaryglobalrescaling}),
which \cosmolattice$\,$ understands in terms of the variables $f_{*}$
and $\omega_{*}$ defined in (\ref{eq:definitionfstaromegastar}).
For the Palatini case, they are given by

\begin{equation}
f_{*}=M_{P}\quad\quad\omega_{*}=\frac{\sqrt{\lambda}}{2\xi}M_{P}.\label{cosmolatticerescalingunitarypalatini}
\end{equation}

We separate the study of preheating in the Palatini scenario in two
cases: first without adding the gauge bosons, then with the gauge
bosons to test whether their presence changes the speed of reheating
or not. 

\subsection{Palatini Higgs preheating without gauge bosons}

\subsubsection{Box size and initial conditions }

Just like in the metric case of section \ref{subsec:metricunitsinitial}
we need to estimate the physical lattice size that accomodates best
the sub-Hubble modes that can grow. For this, we can study equation
(\ref{eq:scalarpalatinilinearizedfourier}) numerically and estimate
how efficiently the amplitude $\delta\tilde{\chi}_{\tilde{k}}(\tilde{t})$
increases for a range of momenta $\tilde{k}$. This is a linearized
analysis: we are neglecting any back reaction of the perturbations
on the field. It is however a very good approximation at the very
beginning of preheating because the perturbations are then orders
of magnitude below the energy density scale of the breathing mode
of the inflaton. The range of momenta that was found to become excited
is shown in figure \ref{fig:scalarpalatinimodesthatgrow}.
\begin{figure}

\centering
\input{figures/palatini_scalar_mode_efficiency.pgf}
\vspace{-0.4cm}\caption{(\textit{left}) Evolution of the amplitudes $\delta\tilde{\chi}_{\tilde{k}}(\tilde{t})$
from equation (\ref{eq:scalarpalatinilinearizedfourier}) at different
momentum values. (\textit{right}) Maximum amplitude attained after
some time for a range of momenta. All units are in terms of (\ref{eq:unitaryglobalrescaling}).
\label{fig:scalarpalatinimodesthatgrow}}
\end{figure}
We then set the longest mode $\tilde{k}_{IR}\to80$, which in turn
yields a box size of $\tilde{L}\to2\pi/\tilde{k}_{IR}\approx0.0785.$ 

\subsubsection{Run without gauge bosons}

\label{subsec:palatinilatticewithoutbosons}We are now ready to delegate
the heavy grinding to \cosmolattice$\,$ again. Like before we need
to input the Einstein frame potential in program units. Combining
(\ref{eq:einsteinframepalatinipotential}), (\ref{cosmolatticerescalingunitarypalatini})
and the idea of the transformation (\ref{eq:einsteinframepotentialtoprogramunits}),
we get the following potential:

\begin{equation}
\tilde{V}_{E}(\tilde{\chi})=\tanh^{4}\left(\sqrt{\xi}\tilde{\chi}\right).\label{eq:palatinieinsteinframepotentialnogaugeprogramunits}
\end{equation}
The summary of the run with potential (\ref{eq:palatinieinsteinframepotentialnogaugeprogramunits})
is given in the simulation box \ref{simulation:palatininobosons}.\fancytab[\centering]{palatininobosons}%
{Palatini preheating without gauge bosons.}%
{% 	
\begin{tabular}{rlrl}  	
 \multicolumn{4}{c}{Model: $\texttt{higgs\_palatini}$}  \\
 \midrule 	
 $N$   &    256 &     $\tilde{\Delta t}$ & $10^{-5}$ \\
 $\tilde{k}_\mathrm{IR}$ & 80 & $\tilde{k}_\mathrm{cut-off}$ & 1000 \\
 $\chi(0)$ & $9.41\cdot 10^{14}$ & $\dot{\chi}(0)$ & $-9.35 \cdot 10^{27} $ \\
 evolver & Velocity Verlet 2$^\mathrm{nd}$ order & $\tilde{t}_\mathrm{max}$ & 2 \\
 \midrule  	
core hours & 770 & relative energy conservation & $1.0\cdot10^{-3}$\\ 
\end{tabular}%  
}{ht}

\paragraph{Field dynamics and energies}

The situation is completely different in the Palatini scenario. The
tachyionic mechanism is a lot more efficient than the gauge boson
production of the metric case as we can see from the left side of
figure \ref{fig:scalarpalatinifieldandenergies}. The zero-mode of
the inflaton condensate is damped down in less than a full oscillation.
We see in the right part of figure \ref{fig:scalarpalatinifieldandenergies}
the exponential growth of the energy contained in the gradient of
the Higgs field: at the end of the first oscillation, a third of the
total energy of the system is contained in the gradient. From there,
the Universe is already dominated by tachyonic excitations.

\paragraph{Spectrum of excitations and lattice effects}

Even if at this point the Universe is dominated by excitations, the
entropy is not yet maximal. Indeed the tachyonic mechanism favors
very specific momenta, making the spectrum of the resulting perturbations
very peaked. This can be seen in the left part of figure \ref{fig:spectrascalarhiggs},
where we recognize as well the peak of figure \ref{fig:scalarpalatinimodesthatgrow}
-- that is a narrow peak of tachyonic excitations centered around
$\tilde{k}\approx500$. Now because the tachyonic modes are monochromatic
waves with a very long wavelength, an additional phase of energy transfer
should still take place. Its onset can be seen as the power spectrum
of the perturbations broadens (right part of figure \ref{fig:spectrascalarhiggs}).
This phase, albeit a bit slower than the explosive first part, still
completes within a few oscillations of the inflaton at most. 
\begin{figure}[H]
\centering
\input{figures/scalarpalatinibeginning.pgf}
\vspace{-0.4cm}

\caption{(\textit{left}) Evolution of the field average right after the end
of slow roll. The breathing mode of the Higgs condensate is quickly
quenched as its energy is transferred to the growing perturbations.
(\textit{right}) Fraction of the total energy of the system contained
in the gradient of the Higgs field. All units are in terms of (\ref{eq:unitaryglobalrescaling}).
\label{fig:scalarpalatinifieldandenergies}}

\end{figure}
\begin{figure}[H]
\centering
\input{figures/scalarpalatinispectra.pgf}
\vspace{-0.4cm}

\caption{(\textit{left}) Growth of the tachyonic peak at the beginning of preheating.
(\textit{right}) At later times, thermalization of the system. Everything
is in units of (\ref{eq:unitaryglobalrescaling}). \label{fig:spectrascalarhiggs}}
\end{figure}


\paragraph{Reaching radiation domination}

The scale factor and equation of state are plotted in figure \ref{fig:scalefactorandstateequationscalarpalatinipreheating}.
We see that even without considering the possible decay to fermions
or the Higgsed gauge bosons, the equation of state approches $1/3$
in a negligible amount of e-folds. Adding the possibility of decays
to lighter particles or adding more non-perturbative ways of depleting
the initial condensate would likely only make things faster -- hence
we feel as though this lattice simulation sets a lower-bound on the
temperature of reheating (or a higher-bound on the time of reheating).

\begin{figure}[h]
\centering
\input{figures/scalarpalatinieqstateandscalefactor.pgf}
\vspace{-0.4cm}

\caption{(\textit{left}) Evolution of the scale factor (\textit{right}) Equation
of state. Everything is in units of (\ref{eq:unitaryglobalrescaling}).\label{fig:scalefactorandstateequationscalarpalatinipreheating}}
\end{figure}


\subsection{Palatini Higgs preheating with scalarized gauged bosons}

\label{subsec:Palatini-Higgs-preheating-gauge-bosons-lattice}This
will be very similar to the simulation of \ref{subsec:metricunitsinitial}.
The setup will be exactly the same, at the exception of
\begin{itemize}
\item the Einstein frame potential which is different in the Palatini scenario,
\item and as a result the units which will be chosen differently. 
\end{itemize}
\begin{figure}
\centering
\input{figures/parametricresonanceillustration.pgf}
\vspace{-0.4cm}

\caption{Numerical evolution of a Fourier mode from equation (\ref{eq:palatinibosonfourierevolution}).
\label{fig:parametricresonanceillustration}}
\end{figure}
Combining the potential (\ref{eq:toteinsteinpotential}) and the units
(\ref{cosmolatticerescalingunitarypalatini}), we obtain the following
program potential:
\begin{equation}
\tilde{V}_{E}^{\mathrm{tot}}\big(\tilde{\chi},\{\tilde{W}_{i}\}\big)=\tanh^{4}\left(\sqrt{\xi}\tilde{\chi}\right)+\frac{\xi}{2\lambda}\frac{\tanh\left(\sqrt{\xi}\tilde{\chi}\right)^{2}}{\cosh\left(\sqrt{\xi}\tilde{\chi}\right)^{2}}\sum_{i}g_{i}^{2}\tilde{W}_{i}^{2}.\label{eq:unitarypalatinitotalpotentialgaugebosons}
\end{equation}
Its derivatives and its implementation in \cosmolattice$\,$ are provided
in appendix \ref{sec:appendixscalarpalatini}. Studying the perturbations
in Fourier space, i.e.
\begin{equation}
\tilde{W}(x,t)\to\tilde{W}(t)+\delta\tilde{W}_{\tilde{k}}(t)e^{i\tilde{k}\tilde{x}}
\end{equation}
the first order evolution reads
\begin{equation}
\delta\ddot{\tilde{W}}_{\tilde{k}}+(3-\alpha)a^{2\alpha}\mathcal{\tilde{H}}\delta\dot{\tilde{W}}_{\tilde{k}}+a^{2\alpha}\Bigg(\underbrace{\frac{\tilde{k}^{2}}{a^{2}}+\frac{d^{2}\tilde{V}_{E}^{\mathrm{tot}}}{d^{2}\tilde{W}_{\tilde{k}}}}_{\tilde{\omega}_{\tilde{k}}^{2}}\Bigg)\delta\tilde{W}_{\tilde{k}}=0\label{eq:palatinibosonfourierevolution}
\end{equation}
with 
\begin{equation}
\frac{d^{2}\tilde{V}_{E}^{\mathrm{tot}}}{d^{2}\tilde{W}_{\tilde{k}}}=\frac{g^{2}\xi}{\lambda}\frac{\tanh\left(\sqrt{\xi}\tilde{\chi}\right)^{2}}{\cosh\left(\sqrt{\xi}\tilde{\chi}\right)^{2}}.
\end{equation}
We see that there cannot be a simple tachyonic excitation of a given
mode $\delta\tilde{W}_{\tilde{k}}$ since $\tilde{\omega}_{\tilde{k}}^{2}$
is always positive. However, as first detailed in~\cite{Kofman1997}
the phenomenon of parametric resonance can still produce particles.
The frequency $\tilde{\omega}_{\tilde{k}}$ decreases strongly for
small values of $\tilde{\chi}$, which results in the breach of the
adiabacity condition
\begin{equation}
\frac{d\tilde{\omega}_{\tilde{k}}}{d\tilde{t}}\ll\tilde{\omega}_{\tilde{k}}^{2}.\label{eq:adiabacitycondition}
\end{equation}
Whenever (\ref{eq:adiabacitycondition}) is respected, $\delta\tilde{W}_{\tilde{k}}$
oscillates fast at a stable frequency and can thereby be evolved semiclassicaly
(conservation of the number of particles). Since (\ref{eq:adiabacitycondition})
is not respected for small $\tilde{\chi}$ values, there will be a
change in the number of excitations at each zero-crossing of $\tilde{\chi}$.
This can be seen in the numerical evolution of (\ref{eq:palatinibosonfourierevolution}),
plotted in figure (\ref{fig:parametricresonanceillustration}). We
see how the frequency of the $\delta\tilde{W}$ line changes abruptly
whenever the inflaton crosses 0, resulting in an increase of the amplitude
of the mode. 

Let us estimate which modes can grow from the condition (\ref{eq:adiabacitycondition}).
Expliciting the differentiation $d\tilde{\omega}_{\tilde{k}}/dt$,
we get
\begin{equation}
\frac{d\tilde{\omega}_{\tilde{k}}}{d\tilde{t}}=\frac{1}{\tilde{\omega}_{\tilde{k}}}\frac{d^{3}\tilde{V}_{E}^{\mathrm{tot}}}{d^{2}\tilde{W}_{\tilde{k}}d\tilde{\chi}}\dot{\tilde{\chi}}.
\end{equation}
Inserting it back in (\ref{eq:adiabacitycondition}) yields the adiabacity
condition
\[
\frac{d^{3}\tilde{V}_{E}^{\mathrm{tot}}}{d^{2}\tilde{W}_{\tilde{k}}d\tilde{\chi}}\dot{\tilde{\chi}}\ll\Bigg(\frac{\tilde{k}^{2}}{a^{2}}+\frac{d^{2}\tilde{V}_{E}^{\mathrm{tot}}}{d^{2}\tilde{W}_{\tilde{k}}}\Bigg)^{3/2}.
\]
Now around $\tilde{\chi}\approx0$, the second derivative of $\tilde{V}_{E}^{\mathrm{tot}}$
is quadratic in $\tilde{\chi}$ and is thus negligible next to large
enough $\tilde{k}^{2}.$ We are left with the following condition
on the momenta that can breach the adiabacity:
\[
\frac{\tilde{k}^{3}}{a^{3}}\ll\left\vert \frac{d^{3}\tilde{V}_{E}^{\mathrm{tot}}}{d^{2}\tilde{W}_{\tilde{k}}d\tilde{\chi}}\dot{\tilde{\chi}}\right\vert 
\]
In the first zero-crossing of the inflaton we have $\tilde{\rho}_{\mathrm{tot}}=\frac{1}{2}\dot{\tilde{\chi}}^{2}$,
and in our units $\tilde{\rho}_{tot}\approx1$ from equation (\ref{eq:unitarypalatinitotalpotentialgaugebosons}).
With $a=1$ the estimation is

\[
\tilde{k}^{3}\ll\sqrt{2}\left\vert \frac{d^{3}\tilde{V}_{E}^{\mathrm{tot}}}{d^{2}\tilde{W}_{\tilde{k}}d\tilde{\chi}}\right\vert \approx2\sqrt{2}\frac{g^{2}\xi^{2}}{\lambda}\vert\tilde{\chi}\vert
\]
With an upper limit chosen from preliminary simulations of $\vert\tilde{\chi}\vert\to0.0001$
in which the boson production happens, we have $\tilde{k}\ll20000$.
This is a very large upper bound, so we will rather numerically integrate
(\ref{eq:palatinibosonfourierevolution}) at different $\tilde{k}$
values to obtain a stricter estimate. It seems that the effective
range will more likely be anything below $\tilde{k}\sim3000$. This
value is to be compared with the momentum effective in tachyonic preheating,
that is $\tilde{k}\sim500$. This is making the lattice simulation
challenging as the more different the ranges of $\tilde{k}$ on which
the two mechanisms operate, the larger the lattice should be to be
able to accomodate all the modes.

\paragraph{Box size}

Preliminary lattice simulations with the bosons show that the $256^{3}$
box used when neglecting the gauge bosons in section \ref{subsec:palatinilatticewithoutbosons}
cannot suffice anymore. Moreover, the value $\tilde{k}_{\mathrm{IR}}=80$
chosen there, which allowed for an optimal expression of the tachyonic
peak, is too small here and would require a very dense (large $N$)
lattice for UV modes energetic enough. The $Z$ boson and its larger
coupling strength in particular, tend to quickly migrate towards very
short modes. Thus simulating the two mechanisms on the same lattice
will require
\begin{itemize}
\item a larger $\tilde{k}_{\mathrm{IR}}$ -- a compromise must be made
such that the tachyonic excitations are still well represented,
\item and a $512^{3}$ sized simulation box. 
\end{itemize}
The $256^{3}$ box could still be ran on a standard desktop computer
with a recent quad-core CPU -- this is not possible anymore with
a $512^{3}$ box. Such a large box is even at the limit of what we
can reasonably run on the EPFL SCITAS cluster. Larger boxes would
be technically possible, but their computational cost would most probably
exceed the value added to the $512^{3}$ box result. 

\begin{figure}[H]
\centering
\input{figures/palatini_optimal_kir.pgf}
\vspace{-0.4cm}

\caption{(\textit{Left}) Evolution of the energy contained in the gradient
of the $\tilde{\chi}$ field for several physical sizes of the simulation
box. (\textit{Right}) Zoom on the upper right part of the left plot.
\label{fig:palatini_optimal_kir}}
\end{figure}


\paragraph{Choosing $\tilde{k}_{\mathrm{IR}}$}

A series of simulations without gauge bosons at different values between
$\tilde{k}_{\mathrm{IR}}=80$ and $\tilde{k}_{\mathrm{IR}}=480$ were
ran in a smaller $128^{3}$ box. The idea is to identify the largest
$\tilde{k}_{\mathrm{IR}}$ that still allows for a very efficient
tachyonic preheating. No dramatic difference was found between the
$\tilde{k}_{\mathrm{IR}}$'s smaller than 400. The growth of the energy
contained in the gradient is displayed in figure \ref{fig:palatini_optimal_kir}
for the other, larger momenta. We see that the energy becomes progressively
less efficient as $\tilde{k}_{\mathrm{IR}}$ increases, but the real
jump happens between $\tilde{k}_{\mathrm{IR}}=460$ and $\tilde{k}_{\mathrm{IR}}=480$.
We will thereby use the former for our simulation with bosons. 

\subsection{Run with scalarized gauge bosons}

The box \ref{simulation:palatiniwithbosons} gives the summary of
the simulation of Palatini-Higgs preheating with scalarized gauge
bosons. Because of the cost of evolving the equations on such a large
box, we also limited ourselves to the short times leading to the onset
of radiation domination and did not evolve the lattice until the equation
of state converges to $1/3$. 

\fancytab[\centering]{palatiniwithbosons}%
{Palatini preheating with scalarized gauge bosons.}%
{% 	
\begin{tabular}{rlrl}  	
 \multicolumn{4}{c}{Model: $\texttt{higgs\_palatini\_gauge\_scalars}$}  \\
 \midrule 	
 $N$   &    512 &     $\tilde{\Delta t}$ & $4\cdot 10^{-6}$ \\
 $\tilde{k}_\mathrm{IR}$ & 460 & $\tilde{k}_\mathrm{cut-off}$ & 3000 \\
 $\chi(0)$ & $3.05\cdot 10^{15}$ & $\dot{\chi}(0)$ & $-6.37 \cdot 10^{26} $ \\
 evolver & Velocity Verlet 2$^\mathrm{nd}$ order & $\tilde{t}_\mathrm{max}$ & 0.2 \\
 \midrule  	
core hours & 2250 & relative energy conservation & $3.9\cdot10^{-3}$\\ 
\end{tabular}%  
}{ht}

\paragraph{Field dynamics and energies}

As expected the behavior is extremely similar to that of the bosonless
run, because the perturbations in the gauge fields increase at a rate
orders of magnitude below that of the tachyonic excitations. This
is shown in figure \ref{fig:scalarpalatinibosonsfieldandenergies},
where on the left we plotted the trajectory of the average of the
inflaton field, and on the right we plotted the evolution of the relevant
components of the energy density. The resemblance of the left parts
of figures \ref{fig:scalarpalatinifieldandenergies} and \ref{fig:scalarpalatinibosonsfieldandenergies}
is striking because there is no significant difference in the time
at which backreation starts occurring. 

Regarding the evolution of the energy densities of the gauge bosons,
we see in the right part of figure \ref{fig:scalarpalatinibosonsfieldandenergies}
how the simple analysis of parametric resonance only holds at first.
Indeed from $\tilde{t}=0.022$ and on, the densities of the bosons
are dragged by the tachyonic excitations. Now according to Rubio and
Tomberg, the gauge bosons never accumulate because the perturbative
decay to Standard Model fermions is efficient enough such that the
population of bosons is completely depleted between each zero-crossing
of the inflaton. This should not hold anymore in the nonlinear regime,
because the production of the gauge bosons due to the excitations
already present in the inflaton field is orders of magnitude faster
than parametric resonance. 

\begin{figure}
\centering
\input{figures/scalarpalatinibosonsbeginning.pgf}
\vspace{-0.4cm}

\caption{Higgs-Palatini preheating with scalarized gauge bosons. (\textit{left})
Evolution of the inflaton field average right after the end of slow
roll. (\textit{right}) Fraction of the total energy of the system
contained in the gradient of the Higgs field, as well as the energy
density fraction contained in the boson fields. All units are in terms
of (\ref{eq:unitaryglobalrescaling}). \label{fig:scalarpalatinibosonsfieldandenergies}}
\end{figure}


\paragraph{Spectrum of excitations and lattice effects}

The spectra of the perturbations in the inflaton field (left part
of figure \ref{fig:spectrascalarhiggspalatinibosons}) are almost
identical to those of the bosonless case, whereas the character of
the spectra of the perturbations in the boson fields (right part of
figure \ref{fig:spectrascalarhiggspalatinibosons}) is very similar
to what we had in the metric case (figure \ref{fig:metricspectra}).
The excitations in all the fields were well contained on the lattice
throughout the simulation, except for those of the $Z$ boson that
started to accumulate at the UV after $\tilde{t}\sim0.18$. However
this has no consequence regarding our conclusions. 
\begin{figure}
\centering
\input{figures/scalarpalatinibosonsspectra.pgf}
\vspace{-0.4cm}

\caption{Spectra of excitations in Higgs-Palatini preheating with scalarized
gauge bosons. (\textit{left}) Growth of the tachyonic peak at the
beginning of preheating. (\textit{right}) The spectra on the side
of the scalarized gauge bosons. Everything is in units of (\ref{eq:unitaryglobalrescaling}).
\label{fig:spectrascalarhiggspalatinibosons}}
\end{figure}


\paragraph{Reaching radiation domination}

Now the biggest question is whether the addition of the bosons helps
with reaching radiation domination even faster. In figure \ref{fig:scalarpalatinieqstatewithvswithoutbosons}
we plot the evolution of the equation of state of the two simulations,
without and with gauge bosons. We see that there is again no significant
difference between the two, signaling that if we want an even faster
convergence of the equation of state we will need a more evolved model,
perhaps with the inclusion of the decay to Standard Model fermions
or by considering all the degrees of freedom of the Higgs doublet.

\begin{figure}
\centering
\input{figures/scalarpalatinieqstatewithvswithoutbosons.pgf}
\vspace{-0.4cm}

\caption{Comparison of the evolution of the equation of state of simulations
\ref{simulation:palatininobosons} and \ref{simulation:palatiniwithbosons}.
Everything is in units of (\ref{eq:unitaryglobalrescaling}). \label{fig:scalarpalatinieqstatewithvswithoutbosons}}
\end{figure}


\section{Cosmological observables}

Depending on the requested precision on the cosmological observables,
the details of preheating are not important and things can be calculated
from the shape of the inflaton potential only. As mentionned in the
introduction however, the thermal history following inflation matters
as well. For a correction to the next order of accuracy, the reheating
epoch can be parametrized with the number of e-folds of reheating
$N_{\mathrm{reh}}$ as well as an effective equation of state defined
as
\begin{equation}
w_{\mathrm{eff}}\coloneqq\frac{1}{N_{\mathrm{reh}}}\int_{0}^{N_{\mathrm{reh}}}dN'\,w(N')=\frac{1}{N_{\mathrm{reh}}}\int_{a_{\mathrm{end}}}^{a_{\mathrm{reh}}}\frac{da}{a}\,w(a)\label{eq:defeffectiveeqofstate}
\end{equation}
where $a_{\mathrm{end}}=1$ and $a_{\mathrm{reh}}$ are the scale
factors at the end of inflation and at the end of preheating respectively.
The temperature of reheating then reads~\cite{Saha2020}
\begin{equation}
T_{\mathrm{reh}}=\left(\frac{30\rho_{\mathrm{co}}}{g_{\mathrm{reh}}\pi^{2}}\right)^{1/4}\exp\Bigg[-\frac{3}{4}\big(1+w_{\mathrm{eff}}\big)N_{\mathrm{re}}\Bigg].\label{eq:effectivereheatingtemperature}
\end{equation}
Where $\rho_{\mathrm{co}}$ is the energy density at the end of the
epoch where the field oscillates coherently and $g_{\mathrm{reh}}=106.75$
is the effective number of degrees of freedom at the end of reheating.
For our purpose we define the end of reheating as the moment when
the equation of state $w$ becomes greater than $0.3$. In particular,
the simulation of box \ref{simulation:palatininobosons} yields the
following
\[
N_{\mathrm{reh}}\approx0.13\;\quad w_{\mathrm{eff}}=0.04\;\quad T_{\mathrm{reh}}\approx0.89\,T_{\mathrm{reh}}^{\mathrm{instant}}
\]
where 
\[
T_{\mathrm{reh}}^{\mathrm{instant}}=\left(\frac{30\lambda}{4\pi^{2}\xi^{2}g_{*\mathrm{reh}}}\right)^{1/4}M_{P}
\]
is the temperature of an instantaneous reheating.~\cite{rubio2019}
This slightly lower temperature of reheating translates into a slightly
lower number of e-folds $N_{*}$ left from the pivot scale $k_{*}$
to the end of inflation. This makes the spectral shift 
\[
n_{s}\approx1-\frac{2}{N_{*}}-\frac{0.8}{N_{*}^{2}}
\]
larger by $\sim0.0001$, which is negligible and essentially does
not change the result of an instant preheating calculated by Rubio. 


