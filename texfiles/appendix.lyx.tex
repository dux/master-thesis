\chapter{Metric potential derivatives}\label{sec:appendixscalarmetric}The
total potential in the Einstein frame and in program units is
\begin{equation}
\tilde{V}_{E}^{\mathrm{tot}}\big(\tilde{\chi},\{\tilde{W}_{i}\}\big)=\frac{3}{2}\Big(1-e^{-\sqrt{2/3}\vert\tilde{\chi}\vert}\Big)^{2}+\frac{3}{4}\frac{\xi}{\lambda}\Big(1-e^{-\sqrt{2/3}\vert\tilde{\chi}\vert}\Big)\sum_{i=1}^{3}g_{i}^{2}\tilde{W}_{i}^{2}
\end{equation}
The derivatives with respect to the inflaton are
\begin{equation}
\frac{d\tilde{V}_{E}^{\mathrm{tot}}}{d\tilde{\chi}}=\frac{e^{-2\sqrt{2/3}\vert\tilde{\chi}\vert}\Big(e^{\sqrt{2/3}\vert\tilde{\chi}\vert}\big(4\lambda+\xi\sum_{i=1}^{3}g_{i}^{2}\tilde{W}_{i}^{2}\big)-4\lambda\Big)}{4\sqrt{6}\xi^{2}}\frac{\tilde{\text{\ensuremath{\chi}}}}{\vert\tilde{\chi}\vert},
\end{equation}
\begin{equation}
\frac{d^{2}\tilde{V}_{E}^{\mathrm{tot}}}{d\tilde{\chi}^{2}}=\frac{e^{-2\sqrt{2/3}\vert\tilde{\chi}\vert}\Big(8\lambda-e^{\sqrt{2/3}\vert\tilde{\chi}\vert}\big(4\lambda+\xi\sum_{i=1}^{3}g_{i}^{2}\tilde{W}_{i}^{2}\big)\Big)}{12\xi^{2}}.
\end{equation}
The derivatives with respect to the scalarized gauge fields read
\begin{equation}
\frac{d\tilde{V}_{E}^{\mathrm{tot}}}{d\tilde{W_{j}}}=\frac{1-e^{-\sqrt{2/3}\vert\tilde{\chi}\vert}}{4\xi}g_{j}^{2}\tilde{W}_{j},
\end{equation}

\begin{equation}
\frac{d^{2}\tilde{V}_{E}^{\mathrm{tot}}}{d\tilde{W}_{j}^{2}}=\frac{1-e^{-\sqrt{2/3}\vert\tilde{\chi}\vert}}{4\xi}g_{j}^{2}.
\end{equation}
The implementation example is given for the Palatini case in the next
appendix. 

\chapter{Palatini potential derivatives and \cosmolattice$\,$ implementation}

\label{sec:appendixscalarpalatini}As stated in the main text, the
program potential is as follows

\begin{equation}
\tilde{V}_{E}^{\mathrm{tot}}\big(\tilde{\chi},\{\tilde{W}_{i}\}\big)=\tanh^{4}\left(\sqrt{\xi}\tilde{\chi}\right)+\frac{\xi}{2\lambda}\frac{\tanh^{2}\left(\sqrt{\xi}\tilde{\chi}\right)}{\cosh^{2}\left(\sqrt{\xi}\tilde{\chi}\right)}\sum_{i=1}^{3}g_{i}^{2}\tilde{W}_{i}^{2}.\label{eq:appendixfullpalatinipotential}
\end{equation}
The derivatives with respect to the inflaton are
\begin{equation}
\frac{d\tilde{V}_{E}^{\mathrm{tot}}}{d\tilde{\chi}}=\frac{\sqrt{\xi}}{2\lambda}\frac{8\lambda\sinh^{2}\left(\sqrt{\xi}\tilde{\chi}\right)+\xi\Big(3-\cosh\left(2\sqrt{\xi}\tilde{\chi}\right)\Big)\sum_{i}g_{i}^{2}\tilde{W}_{i}^{2}}{\cosh^{4}\left(\sqrt{\xi}\tilde{\chi}\right)}\tanh\left(\sqrt{\xi}\tilde{\chi}\right),\label{eq:appendixpalatinidvdchi}
\end{equation}
\begin{multline}
\frac{d^{2}\tilde{V}_{E}^{\mathrm{tot}}}{d\tilde{\chi}^{2}}=\frac{\xi}{\lambda}\frac{1}{\cosh^{4}\left(\sqrt{\xi}\tilde{\chi}\right)}\Bigg[\cosh\left(2\sqrt{\xi}\tilde{\chi}\right)-10\tanh^{2}\big(\sqrt{\xi}\tilde{\chi}\big)\Big)\xi\sum_{i=1}^{3}g_{i}^{2}\tilde{W}_{i}^{2}\\
-4\lambda\Big(\cosh\left(2\sqrt{\xi}\tilde{\chi}\right)-4\Big)\tanh^{2}\big(\sqrt{\xi}\tilde{\chi}\big)\Bigg].\label{eq:appendixpalatinid2vdchi2}
\end{multline}
Things are luckily cleaner with the boson fields derivatives:
\begin{equation}
\frac{d\tilde{V}_{E}^{\mathrm{tot}}}{d\tilde{W_{j}}}=\frac{\xi}{\lambda}\frac{\tanh^{2}\left(\sqrt{\xi}\tilde{\chi}\right)}{\cosh^{2}\left(\sqrt{\xi}\tilde{\chi}\right)}g_{j}^{2}\tilde{W}_{j},\label{eq:appendixpalatinidvdw}
\end{equation}
\begin{equation}
\frac{d^{2}\tilde{V}_{E}^{\mathrm{tot}}}{d\tilde{W}_{j}^{2}}=\frac{\xi}{\lambda}\frac{\tanh^{2}\left(\sqrt{\xi}\tilde{\chi}\right)}{\cosh^{2}\left(\sqrt{\xi}\tilde{\chi}\right)}g_{j}^{2}.\label{eq:appendixpalatinid2vdw2}
\end{equation}
Given the fact that the rest of the action is canonical and standard
(minimally coupled Ricci scalar, canonical kinetic terms for $\tilde{\chi}$)
this potential is the only thing we need to add to \cosmolattice.
One should start from a model file provided with the code itself,
contained in $\texttt{src/models}$, because most of the structure
is always the same and only some parts should be modified or added. 

At the beginning of the file, there is a structure with parameters
that will define the model. In our case, we have 4 scalars; and we
choose to split our potential (\ref{eq:appendixfullpalatinipotential})
in 4 terms so that the program outputs the potential values for each
of the scalars. 

\begin{lstlisting}
struct ModelPars : public TempLat::DefaultModelPars {         
 static constexpr size_t NScalars = 4;                
 static constexpr size_t NPotTerms = 4;
};
\end{lstlisting}

Right after this structure, there is a macro that will define the
name of the model. It should match the name of the file:

\begin{lstlisting}
#define MODELNAME higgs_palatini_gauge_scalars
\end{lstlisting}

Now jumping inside of the class definition, we initialize the storage
of our coupling constants.
\begin{lstlisting}
private:
  double Lambda, xi, g, gz;
\end{lstlisting}
Their actual value will be read off the input file at runtime. This
behavior actually needs to be programmed in the model file as well:
it happens in the brace initialization of the constructor of the model
just below, together with the parsing of the initial values and units
definition. Namely:

\begin{lstlisting}
MODELNAME(  .......  ):
Model<MODELNAME>( ....... )
{
  fldS0 = parser.get<double, 1>("initial_amplitudes");
  piS0  = parser.get<double, 1>("initial_momenta");

  xi     = parser.get<double>("xi");
  Lambda = parser.get<double>("Lambda");
  g      = parser.get<double>("g");
  gz     = parser.get<double>("gz");

  fStar      = 2.435e18;
  omegaStar  = sqrt(Lambda) * 2.435e18 / (2 * xi);
  alpha      = 0;

  setInitialPotentialAndMassesFromPotential();
}
\end{lstlisting}
Here we read the initial value and initial time derivative of the
homogeneous part of the $0^{\mathrm{th}}$field, then read the coupling
constant, and finally defined our units as described in equations
(\ref{eq:unitaryglobalrescaling}) and (\ref{cosmolatticerescalingunitarypalatini}).
Note that we did not provide initial conditions for the gauge scalars:
$\cosmolattice$ defaults to setting their initial average value to
zero if not provided, which is fine in this case. In case they should
have a different initial value, one would need
\begin{lstlisting}
  fldS0 = parser.get<double, 4>("initial_amplitudes");
  piS0  = parser.get<double, 4>("initial_momenta");
\end{lstlisting}
thus reading 4 values on the line of the configuration file with the
corresponding keywords ``initial\_amplitudes'' or ``initial\_momenta''. 

Now we need to add class functions for each the potential terms. Let
us write down the first two:

\begin{lstlisting}
auto potentialterms(Tag<0>)
{
   auto sharg = sqrt(xi) * fldS(0_c);
   return pow<4>(tanh(sharg));
}
\end{lstlisting}
\begin{lstlisting}
auto potentialTerms(Tag<1>)   
{ 
  auto sharg = sqrt(xi) * fldS(0_c);
  auto chipart = 0.5 * xi / Lambda * pow<2>(tanh(sharg)) 
                 / pow<2>(cosh(sharg));
  auto Wpart = pow(g,2) * pow<2>(fldS(1_c));
  return  chipart * Wpart;
}
\end{lstlisting}

Finally the hardest part, that is writing down the first derivatives
(\ref{eq:appendixpalatinidvdchi}), (\ref{eq:appendixpalatinidvdw})
and second derivatives (\ref{eq:appendixpalatinid2vdchi2}), (\ref{eq:appendixpalatinid2vdw2}).
We will only provide two short examples in this document, but the
model file is available for the others. 

\begin{lstlisting}
auto potDeriv(Tag<1>) // derivative w.r.t W+
{ 
  auto sharg = sqrt(xi) * fldS(0_c);
  auto chipart = 0.5 * xi / Lambda * pow<2>(tanh(sharg)) 
                 / pow<2>(cosh(sharg)); 
  return chipart * g*g * fldS(1_c);     
}

(...)

auto potDeriv2(Tag<3>) // 2nd derivative w.r.t Z0
{ 
  auto sharg = sqrt(xi) * fldS(0_c);
  auto chipart = 0.5 * xi / Lambda * pow<2>(tanh(sharg)) 
                 / pow<2>(cosh(sharg)); 
  return chipart * gz*gz;     
}
\end{lstlisting}

\chapter{Non-minimal equations in \cosmolattice}

\label{subsec:cosmolatticeimplementationu1higgs}

\cosmolattice$\,$ can evolve very general theories as long as they
are canonical. All the equations obtained in section \ref{sec:obtaining_dynamical_eqs_nonminimal}
however, are not canonical. Thus specialized classes were created
within the existing framework, which are called only when evolving
the non-minimal equations. Equation (\ref{eq:nonminimalenergyprogramunits})
was added directly to the file responsible for the measurement of
energies with a condition checking whether the model is canonical
or not:

$\;\texttt{cosmointerface/measurements/energiesmeasurer.h}$

Equations (\ref{eq:friedmannh2rescaled}) and (\ref{eq:friedmanaddotnonminimalprogramunits})
have their own files:

$\;\texttt{cosmointerface/evolvers/kernels/scalefactorcomplexhiggskernels.h}$

$\;\texttt{cosmointerface/definitions/hubbleconstraintcomplexhiggs.h}$

Finally, equation (\ref{eq:eomcomplexhiggs}) can be found in 

$\;\texttt{cosmointerface/evolvers/kernels/complexhiggskernels.h}$

The link between these new files is made in the class of the new integrator
described in section \ref{sec:Low-storage-Runge-Kutta}. The evolver
itself can be found in the file 

$\;\texttt{cosmointerface/evolvers/rungekutta.h}$.

For the person interested in reusing this code, it can be found in
the $\texttt{git}$ branch $\texttt{higgspalatini\_non\_minimal\_complex}$
of the \cosmolattice$\,$ main repository. 
