
\section{Lattice prelude: metric Higgs preheating}

As a first test of our lattice methodology using the new code \cosmolattice,
we reproduced parts of a lattice study of a simplified metric Higgs
preheating by Repond and Rubio~\cite{repond2016}. The specific preheating
scenario that we are reproducing is not considered correct anymore
because it does not include the issue of the effective mass spikes
in the longitudinal gauge boson. Thus we are reproducing it for the
purpose of validation of the new code. This section will focus on
the lattice study itself, but a brief rationalization of the mechanism
at play behind the generation of perturbations will be given in section
\ref{subsec:Palatini-Higgs-preheating-gauge-bosons-lattice}. 

\subsection{Key differences with Palatini preheating}

Metric Higgs preheating has been studied more extensively than its
Palatini counterpart. (See some examples in e.g.~\cite{He2021,GarcaBellido2009,Hamada2021,He2019,repond2016,DeCross2018-1,DeCross2018-2,DeCross2018-3}).
Mainly because metric gravity is the better known formulation, but
also because of all the issues of UV completion that arise in the
metric formulation. However, whether the fact that the UV cut-off
is parametrically higher in the Palatini scenario really makes it
more predictive than the metric equivalent is still an open question
\cite{Shaposhnikov2020}. Leaving these questions aside, the simpler
question of the generation of perturbations after inflation has very
different answers in metric or Palatini Higgs preheating. A surface
summary of the differences in this regard is provided in table \ref{tab:palatinivsmetric}. 
\begin{flushleft}
\begin{table}
\begin{centering}
\caption{Summarized differences between Palatini and metric Higgs preheating.
Note that we are not considering here the troublesome issue of the
longitudinal gauge bosons in metric Higgs inflation -- had this effect
been considered, the metric column could potentially look a lot more
like the Palatini column.\label{tab:palatinivsmetric}}
\par\end{centering}
\centering{}%
\begin{tabular}{>{\raggedright}b{0.29\textwidth}>{\raggedright}b{0.29\textwidth}>{\raggedright}b{0.29\textwidth}}
\hline 
 & Metric & Palatini\tabularnewline
\hline 
Excitation in the Higgs field itself & Very inefficient & Very efficient\tabularnewline[0.4cm]
Parametric resonance (gauge bosons) & Efficient & Efficient\tabularnewline[0.4cm]
number of $e$-folds taken up by reheating & Probably several (see figure \ref{fig:metricscalefactorandeqofstate}) & Negligible\tabularnewline[0.4cm]
Reheating temperature & Probably lower than the scale of inflation & Probably close to the scale of inflation\tabularnewline
\hline 
\end{tabular}
\end{table}
\par\end{flushleft}

\subsection{Reproducing a metric preheating lattice study}

We now start discussing the ingredients required for the replication
of the metric lattice study by Repond and Rubio.

\subsubsection{Metric dynamical equations}

The system to be reproduced has the inflaton field as well as 3 scalarized
gauge bosons. The procedure that yields the equations of motion of
the different scalars and of the scale factor is exactly the same
as what we did in the Palatini case in sections \ref{subsec:palatini_inflaton_eom},
\ref{subsec:Palatini-Gauge-bosons} and \ref{subsec:friedmanneqsscalars}.
However, the resulting Einstein frame potential will be different
because the metric formulation Ricci tensor transforms under the Weyl
rescaling (\ref{eq:weyl_transformation_general}). We will not go
through the procedure again and instead we just give the result in
equation (\ref{eq:metricfieldeom}):

\begin{equation}
\ddot{\phi}-\frac{1}{a^{2}}\nabla^{2}\phi+3H\dot{\phi}+\frac{d}{d\phi}\Bigg(V_{E}(\chi)+\sum_{i=1}^{3}\frac{1}{2}m_{B_{i}}^{2}(\chi)B_{i}^{2}\Bigg)=0\label{eq:metricfieldeom}
\end{equation}
where $B_{i}$ are the gauge bosons, $m_{B_{i}}(\chi)$ their corresponding
mass, $V_{E}$ the metric Einstein frame potential of the inflaton.
$\phi$ can be either the inflaton $\chi$ or one of the gauge bosons.
Repond and Rubio also considered an extension where the gauge bosons
$B_{i}$ can perturbatively decay to relativistic fermions -- we
will skip this part as it does not bring an added value to the validation
of our method. The Einstein frame potential $V_{E}(\chi)$ and masses
$m_{B_{i}}^{2}(\chi)$ read:
\begin{align}
V_{E}(\chi) & \approx\frac{1}{2}\frac{\lambda}{3}\frac{M_{P}^{2}}{\xi^{2}}\chi^{2},\label{eq:metricformulationeinsteinframepotential}\\
m_{B_{i}}^{2}(\chi) & =\frac{g_{i}^{2}M_{P}^{2}\Big(1-e^{-\sqrt{2/3}\vert\chi\vert/M_{P}}\Big)}{4\xi}.
\end{align}
Note that the constant $\alpha$ (see equation \ref{eq:friedmannmetric})
does not appear at all in (\ref{eq:metricfieldeom}): we set it to
zero because it is the most convenient choice for a potential quadratic
near its origin. See~\cite[eq. (455) and the paragraph before it for the rational behind this]{cosmolattice2020}. 

\subsubsection{Units and initial conditions of the metric simulation}

\label{subsec:metricunitsinitial}We also need to reproduce the units
and rescaling used in the paper by Repond and Rubio. We will adopt
the same rescaling of the fields, time and space:
\[
\chi\to M_{P}\tilde{\chi}\quad\quad t\to a^{\alpha}\frac{\tilde{t}}{\sqrt{\frac{\lambda}{6}}\frac{M_{P}}{\xi}}\quad\quad x\to\frac{\tilde{x}}{\sqrt{\frac{\lambda}{6}}\frac{M_{P}}{\xi}}
\]
Now \cosmolattice$\,$ understands units in terms of the field rescaling
$f_{*}$ and spacetime rescaling $\omega_{*}$ defined as follows:
\begin{equation}
\chi\to f_{*}\tilde{\chi}\quad\quad t\to a^{\alpha}\frac{\tilde{t}}{\omega_{*}}\quad\quad x\to\frac{\tilde{x}}{\omega_{*}}.\label{eq:definitionfstaromegastar}
\end{equation}
Thus we want the following:
\begin{equation}
f_{*}=M_{P}\quad\quad\omega_{*}=\sqrt{\frac{\lambda}{6}}\frac{M_{P}}{\xi}.\label{cosmolatticerescalingmetric}
\end{equation}
Rewriting (\ref{eq:metricfieldeom}) in terms of (\ref{cosmolatticerescalingmetric})
yields the same equation with a rescaled potential, called from hereon
the program potential. 
\begin{align}
\tilde{V}_{E}(\tilde{\chi}) & =\frac{V_{E}(M_{P}\tilde{\chi})}{f_{*}^{2}\omega_{*}^{2}}=\tilde{\chi}^{2}\label{eq:einsteinframepotentialtoprogramunits}\\
\tilde{m}_{B_{i}}^{2}(\tilde{\chi}) & =\frac{m_{B_{i}}^{2}(M_{P}\tilde{\chi})}{\omega_{*}^{2}}=\frac{3\xi}{2\lambda}g_{i}^{2}{}_{P}\big(1-e^{-\sqrt{2/3}\vert\tilde{\chi}\vert)}\big)
\end{align}
Indeed dividing the potential term by $f_{*}^{2}\omega_{*}^{2}$ yields
the same as manually rescaling fields, time and space in the equation
itself. The expression of the potential itself will be used by \cosmolattice$\,$
to source the potential part of equations (\ref{eq:simplescalardensity})
and (\ref{eq:simplescalarpressure}) and in turn the Friedmann equations.
To evolve the field equations (\ref{eq:metricfieldeom}) we will also
need the first derivative of the total potential with respect to each
field. Moreover the second derivative of the total potential with
respect to each field is also needed in the generation of the initial
fluctuations. All these expressions are provided in appendix \ref{sec:appendixscalarmetric}. 

We also need to decide on the size of the cosmological box we would
like to simulate. The size must be such that the modes of interest
(those that will grow) are well represented on the lattice. Here again,
we simply take the length chosen by Repond and Rubio:
\begin{equation}
L=\frac{1}{2}\;\Bigg(\sqrt{\frac{\lambda}{3}}\frac{M_{P}}{\xi}\Bigg)^{-1}.
\end{equation}
With the values of the parameters
\[
\lambda=0.0034\quad\quad\xi=1500\quad\quad g=0.54\quad\quad g_{Z}=0.70,
\]
in our program units the box size translates to
\begin{equation}
\tilde{L}=\omega_{*}L=\frac{1}{2\sqrt{2}}\approx0.354.
\end{equation}
The longest mode that can be represented on the lattice is
\begin{equation}
\tilde{k}_{\mathrm{IR}}=\frac{2\pi}{\tilde{L}}=4\sqrt{2}\pi\approx17.8.
\end{equation}
The shortest possible mode depends on the size of the grid $N$. Usually,
a power of 2 is chosen for $N$ so that the Fourier transforms are
as fast as they can get. Here we set $N=128$:
\begin{equation}
\tilde{k}_{\mathrm{UV}}=\frac{\sqrt{3}N}{2}\tilde{k}_{\mathrm{IR}}\approx1970.
\end{equation}
which will barely be enough to contain the most energetic modes that
are going to be produced. Of course the bigger $N$ the better, but
the computational cost scales as $N^{3}$: this makes larger lattices
out of the reach even on a cluster. The last piece of information
related to wavenumbers that we need is a cut-off for the initial fluctuations.
Taken from~\cite{repond2016} and translated to our units:
\begin{equation}
\tilde{k}_{\mathrm{cutoff}}\approx197.
\end{equation}
\cosmolattice$\,$ handles the generation of the initial fluctuations
automatically from this cut-off information and the information of
the second derivative of the potential with respect to each field.
The fluctuations (here in the field $\chi$ for example) for each
momentum $k$ below the cut-off are initialized such that their power
spectrum is as follows:
\begin{equation}
\big\langle\vert\delta\chi\vert^{2}\big\rangle=\frac{1}{a^{2}\omega_{k,\chi}},
\end{equation}
with 
\begin{equation}
\omega_{k,\chi}^{2}=k^{2}+a^{2}\frac{\partial^{2}V}{\partial\chi^{2}}.
\end{equation}
The more subtle details of the lattice implementation of the fluctuations
can be found in section 7.1 of~\cite{cosmolattice2020}. 

Finally, the initial conditions on the homogeneous value and momentum
of $\chi$ is given by the slow-roll condition (\ref{eq:generic_slow_roll_condition}).
The initial value of $\chi$ must be chosen such that the inflaton
is about to roll down its potential well. In particular if $\chi(0)$
is chosen too big, the lattice simulation will be stuck in the inflationary
phase for a very long time. In practice, it is convenient to evolve
the homogeneous version of equation (\ref{eq:metricfieldeom}) while
also neglecting the gauge bosons, which are indeed almost inexistant
during inflation:
\begin{equation}
\ddot{\chi}+3H\dot{\chi}+\frac{dV_{E}(\chi)}{d\chi}=0.\label{eq:metricinflationhomogeneouseom}
\end{equation}
(\ref{eq:metricinflationhomogeneouseom}) can be initialized using
the slow-roll condition and evolved using a cheap numerical integrator
until $\chi$ starts rolling down the potential well. The call to
switch to the full lattice evolution can be made when e.g. the kinetic
energy of the system becomes as large as the potential energy, signaling
the end of inflation. 

\subsubsection{Metric lattice results}

\label{subsec:Metric-lattice-results}The idea of this section is
just to make some observations about our lattice results and make
sure that they are similar to those of the study by Repond and Rubio
~\cite{repond2016}. The parameters of the metric simulation are given
in the summary box \ref{simulation:metricwithbosons}. Let us explicit
some of the entries of the box that might not be clear from the text:
$N$ is the size of the side of the lattice, $\tilde{\Delta t}$ the
timestep and $\tilde{t}_{\mathrm{max}}$ the time at which the simulation
stops. Furthermore, all the quantities are given in program units
except for the initial homogeneous components $\chi(0)=f_{*}\tilde{\chi}(0)$
and $\dot{\chi}(0)=f_{*}\omega_{*}\dot{\tilde{\chi}}(0)$, which \cosmolattice$\,$
wants in untransformed units.\fancytab[\centering]{metricwithbosons}%
{Metric preheating with scalar gauge bosons.}%
{% 	
\begin{tabular}{rlrl}  	
 \multicolumn{4}{c}{Model: $\texttt{higgs\_metric}$\footnote{The "Model" corresponds to the name of its \cosmolattice$\,$ implementation.}}  \\
 \midrule 	
 $N$   &    128 &     $\tilde{\Delta t}$ & $3\cdot10^{-4}$ \\
 $\tilde{k}_\mathrm{IR}$ & 15 & $\tilde{k}_\mathrm{cut-off}$ & 160 \\
 $\chi(0)$ & $1.08\cdot 10^{18}$ & $\dot{\chi}(0)$ & $-4.80 \cdot 10^{31} $ \\
 evolver & Velocity Verlet 2$^\mathrm{nd}$ order & $\tilde{t}_\mathrm{max}$ & 2500 \\
 \midrule  	
core hours & 2400 & relative energy conservation\footnote{Calculated automatically by comparison of equations (\ref{eq:friedmanndyn}) and (\ref{eq:friedmannenergy}).} & $3.0\cdot10^{-4}$\\ 
\end{tabular}%  
}{ht}

\paragraph{Field dynamics and energies}

This simplified model of metric Higgs preheating takes long simulations
for the different modes present on the lattice to become thermalized.
Indeed, it takes a lot of oscillations of the inflaton condensate
before the perturbations in the gauge fields become efficient at extracting
energy off the zero mode. 
\begin{figure}[p]
\centering
\input{figures/scalarmetricbeginning.pgf}
\vspace{-0.4cm}

\caption{\label{fig:scalarmetricbeginning}(\textit{left}) Evolution of the
field average right after the end of slow-roll in metric Higgs preheating.
The lattice field average follows the homogeneous case for a while
before the perturbations in the gauge bosons start back-reacting.
(\textit{right}) Fraction of the total energy of the system contained
in the different fields present on the lattice. Notice how everything
is driven by the main oscillation of the inflaton. All the units are
in terms of (\ref{cosmolatticerescalingmetric}).}
\end{figure}
\begin{figure}[p]
\centering
\input{figures/scalarmetriceqstateandscalefactor.pgf}
\vspace{-0.4cm}

\caption{\label{fig:metricscalefactorandeqofstate}(\textit{left}) Increase
of the scale factor as preheating progresses in metric Higgs inflation.
(\textit{right}) Equation of state of the whole system: the dotted
line denotes the radiation domination value $w=1/3$, to which the
system converges. All the units are in terms of (\ref{cosmolatticerescalingmetric}).}
\end{figure}
This can be seen in the left part of figure \ref{fig:scalarmetricbeginning}:
the only efficient damping is of the Hubble type for a solid 8 oscillations.
Even in the subsequent oscillations the extra damping of the zero
mode is mild. 

The right side of figure \ref{fig:scalarmetricbeginning} shows the
evolution of the total energy of the different scalars. Initially,
the expansion of the Universe is still very fast, and the bosons behave
like radiation: $\rho_{W_{i}}a^{4}=\mathrm{constant.}$ Their energy
therefore gets diluted before parametric resonance becomes effective
(after the $8^{\mathrm{th}}$ oscillation) and the resulting exponential
growth overrides the dilution. As expected, the average inflaton energy
density also starts decreasing at this point. Going back to right
after $\tilde{t}=0$, the inflaton energy shows some light oscillation
but is in average constant. This can be explained by considering the
following: throughout the main oscillation, there is an enery transfer
between potential and kinetic energy. When the inflaton energy is
purely potential, the equation of state is that of the cosmological
constant $w=-1$. On the other hand, when the energy is purely kinetic
we have $w=1$. Over the course of an oscillation, the average equation
of state is $\langle w\rangle=0$. This value is coherent with the
value of the effective equation of state of coherent oscillations
\cite{Turner1983}
\begin{equation}
\langle w_{\mathrm{oscillations}}\rangle=\frac{n-1}{n+1},
\end{equation}
with $n=1$ for a potential quadratic near its origin like the one
at hand. This behavior can also be seen directly in the very early
times of the right side of figure \ref{fig:metricscalefactorandeqofstate}.
The same figure also shows how the equation of state keeps oscillating
with a large amplitude for many oscillations, which reveals how long
it takes for this model to reach a stable epoch of radiation domination.
\begin{figure}[H]
\centering
\input{figures/scalarmetricspectra.pgf}
\vspace{-0.4cm}

\caption{\label{fig:metricspectra}Evolution of the spectrum of the perturbations
at different times in metric Higgs preheating. $k_{*}$ is the momentum
favoured by the parametric production of perturbations. All the units
are in terms of (\ref{cosmolatticerescalingmetric}).}
\end{figure}


\paragraph{Spectrum of excitations and lattice effects}

The modes are populated in two phases:
\begin{enumerate}
\item Some excitations are created in the gauge fields around the momentum
$\tilde{k}_{*}$ optimal for parametric resonance.
\item The energy is slowly transfered towards the UV, populating the other
modes.
\end{enumerate}
Figure \ref{fig:metricspectra} shows exactly this. At very short
times ($\tilde{t}/2\pi\sim2-6$) the spectra are peaked around $\text{\ensuremath{\tilde{k}_{*}}}$,
then slowly broaden. The excitations are well contained on the lattice
for most of the run in the sense that the occupation number $n_{\tilde{k}}$
is never significantly populated near $\tilde{k}_{\mathrm{UV}}$.

\paragraph{Lack of perturbative decays to fermions}

The lattice results and conclusions we obtained for metric Higgs preheating
are in line with those of Repond and Rubio. However we are not reproducing
the second part of their analysis where the boson excitations can
decay to fermions. This is very important in metric Higgs preheating
because depleting the boson population makes parametric resonance
less efficient. However, it would require the writing of an extension
to \cosmolattice, and we can argue that the decay to fermions is
less critical to the Palatini scenario. Indeed the preheating mechanism
in the Palatini scenario is the production of tachyionic excitations,
which does not depend on the quantity of excitations already present
in the system. 


