#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 22 17:08:37 2021

@author: fred
"""

output = "/home/fred/outputnoboson_256_kIR80_up2/"
outputbosons = "/home/fred/output_512_kIR460_up0.2/"

import matplotlib as mpl

import matplotlib.pyplot as plt
import numpy as np

from utilities import loadScalarPalatiniNoBosons, loadScalarMetric



t, [phiL, phiLd, phiL2, phiLd2, varphiL, varphiLd], \
   [a, aDot, HL], \
   Ekin0, Egrad0, Vpot0, rho, p0 = loadScalarPalatiniNoBosons(output)


### remove once simulation finished
t = t[:-2]
a = a[:-15]
p0 = p0[:-46]
rho = rho[:-46]
####


eta, r = loadScalarMetric(outputbosons)
tboson = eta


#%%
plt.style.use('masterthesis.mplstyle')
fig, (ax1) = plt.subplots(1,1, figsize=(4,2.6))
ax1.plot(eta, r['p']/r['rho'], label="With bosons")
ax1.plot(t, p0/rho, label="Without bosons")


ax1.set_xlim((0,0.2))
ax1.set_xlabel(r"Time $\tilde{t}$")
ax1.set_ylabel(r"$w(\tilde{t}) = p(\tilde{t}) / \rho(\tilde{t})$")

ax1.legend()
plt.tight_layout()

fig.savefig('../figures/scalarpalatinieqstatewithvswithoutbosons.pgf')