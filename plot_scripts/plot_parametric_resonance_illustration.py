#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun  5 14:30:23 2021

@author: fred
"""


import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl 

taus, phi, W = np.load('parametricresonanceillustration.npy')


plt.style.use('masterthesis.mplstyle')

fig = plt.figure(figsize=(4.5,2.8))
plt.plot(taus, phi, label=r'$\tilde{\chi}(\tilde{t})$')
plt.plot(taus, W, label=r"$\delta\tilde{W}_{\tilde{k}}(\tilde{t})$")
plt.xlabel(r'Time $\tilde{t}$')
plt.legend()
plt.tight_layout()
# plt.show()
mpl.use('pgf')
fig.savefig('../figures/parametricresonanceillustration.pgf')