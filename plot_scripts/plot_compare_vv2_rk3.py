#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  9 18:39:15 2021

@author: fred
"""

import matplotlib as mpl

import matplotlib.pyplot as plt
import numpy as np

from utilities import loadScalarMetric, SpectraOutput


path="/home/fred/cosmolattice2/cosmolattice/build_higgs_palatini_gauge_scalars/"

outputrk = path + 'run_rk3/'
outputvv = path + 'run_vv2/'

eta, rk = loadScalarMetric(outputrk)
eta, vv = loadScalarMetric(outputvv)
t = eta

#%%
# plt.plot(eta, rk['phiL'], eta, vv['phiL'])
plt.style.use('masterthesis.mplstyle')

tmin, tmax = -0.002, 0.052
fig, (ax1,ax2) = plt.subplots(1,2, figsize=(6.1,3.))
ax1.plot(t, vv['phiL'], label=r'$\langle \tilde{\chi} \rangle(t)$ (Velocity Verlet 2)', lw=1)
ax1.plot(t, rk['phiL'], label=r'$\langle \tilde{\chi} \rangle(t)$ (Runge Kutta 3)', ls=':')
ax1.set_xlabel(r'Time $\tilde{t}$')
ax1.set_ylabel(r'Field values')
ax1.set_xlim((tmin, tmax))
ax1.set_ylim((-0.0013, 0.0018))
ax1.legend()


ax2.semilogy(t, vv['a']**3*vv['rhoWp']/vv['rho'][0], label=r"$a^3 \rho_{W^+} / \rho_0$ (Velocity Verlet 2)", lw=1)
ax2.semilogy(t, rk['a']**3*rk['rhoWp']/rk['rho'][0], label=r"$a^3 \rho_{W^+} / \rho_0$ (Runge Kutta 3)", ls=':')

ax2.set_xlim((tmin, tmax))
ax2.set_ylim((2e-8, 0.4))
ax2.set_xlabel(r'Time $\tilde{t}$')
ax2.legend(handlelength=0.9, loc='upper left')
plt.tight_layout()
mpl.use('pgf')
fig.savefig('../figures/comparison_vv2_rk3.pgf')