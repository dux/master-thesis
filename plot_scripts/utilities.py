#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 28 18:52:30 2021

@author: fred
"""
import numpy as np

def loadSingleScalar(path, i):
    average_phi      = np.loadtxt(path+f'average_scalar_{i}.txt', unpack=1)
    eta, phiL, phiLd, phiL2, phiLd2, varphiL, varphiLd = average_phi
    
    average_scale    = np.loadtxt(path+'average_scale_factor.txt', unpack=1)
    _, a, aDot, HL   = average_scale
    
    average_E        = np.loadtxt(path+'average_energies.txt', unpack=1)
    # _, Ekin, Egrad, _, Vpot, Etot = average_energies
    Ekin0,Egrad0     = average_E[1+i:3+i]
    Vpot0, rho       = average_E[3+i:5+i]
    
    p0               = Ekin0 - Egrad0 / 3. - Vpot0
    return eta, [phiL, phiLd, phiL2, phiLd2, varphiL, varphiLd], [a, aDot, HL], Ekin0, Egrad0, Vpot0, rho, p0


def loadScalarPalatiniNoBosons(path):
    return loadSingleScalar(path, 0)

def loadScalarMetric(prefix):

    res = {}
    energy = np.loadtxt(f'{prefix}average_energy_conservation.txt', unpack=1)
    t, rel_diff_friedmann, LHS_friedmann, RHS_friedmann = energy
    res['rel_diff_friedmann']  = rel_diff_friedmann
    res['LHS_friedmann']  = LHS_friedmann
    res['RHS_friedmann'] = RHS_friedmann
    
    
    average_scale    = np.loadtxt(f'{prefix}average_scale_factor.txt', unpack=1)
    _, a, aDot, HL   = average_scale
    res['a'] = a    
    res['aDot'] = aDot
    res['HL'] = HL
    
    
    
    qualifs = ['L', 'Ld', 'L2', 'Ld2', 'varL', 'dvarL']
    fieldnames = ['phi', 'Wp', 'Wm', 'Z']
    for i, field in enumerate(fieldnames):
        average_phi      = np.loadtxt(f'{prefix}average_scalar_{i}.txt', unpack=1)
        for j, qual in enumerate(qualifs):
            res[field+qual] = average_phi[j+1]
        # eta, phiL, phiLd, phiL2, phiLd2, varphiL, varphiLd = average_phi
        # phis = [phiL, phiLd, phiL2, phiLd2, varphiL, varphiLd]


    # average_Wp       = np.loadtxt(f'{prefix}average_scalar_1.txt', unpack=1)
    # _, Wp, Wpd, Wp2, Wpd2, varWp, varWpd = average_Wp
    # Wps = [Wp, Wpd, Wp2, Wpd2, varWp, varWpd ]
    
    # average_Wm       = np.loadtxt(f'{prefix}average_scalar_2.txt', unpack=1)
    # _, Wm, Wmd, Wm2, Wmd2, varWm, varWmd = average_Wm
    # Wms = [Wm, Wmd, Wm2, Wmd2, varWm, varWmd]
    
    # average_Z0       = np.loadtxt(f'{prefix}average_scalar_3.txt', unpack=1)
    # _, Z0, Z0d, Z02, Z0d2, varZ0, varZ0d = average_Z0
    # Zs = [Z0, Z0d, Z02, Z0d2, varZ0, varZ0d]
    

    types = ["Ekin", "Egrad", "Epot"]
    average_E        = np.loadtxt(f'{prefix}average_energies.txt', unpack=1)

    # _, Ekin, Egrad, _, Vpot, Etot = average_energies
    Ekin0,Egrad0, EkinWp,EgradWp, EkinWm,EgradWm, EkinZ, EgradZ = average_E[1:9]
    Vpot0, VpotWp, VpotWm, VpotZ, rho = average_E[9:]
    
    res['Ekin0'] = Ekin0 
    res['Egrad0'] = Egrad0 
    res['EkinWp'] = EkinWp 
    res['EgradWp'] = EgradWp 
    res['EkinWm'] = EkinWm 
    res['EgradWm'] = EgradWm
    res['EkinZ'] = EkinWm 
    res['EgradZ'] = EgradWm    
    res['Vpot0'] = Vpot0 
    res['VpotWp'] = VpotWp 
    res['VpotWm'] = VpotWm
    res['VpotZ'] = VpotZ
    
    res['p0'] = Ekin0 - Egrad0 / 3. - Vpot0
    res['pWp']  = EkinWp - EgradWp / 3. - VpotWp
    res['pWm']  = EkinWm - EgradWm / 3. - VpotWm
    res['pZ']  = EkinZ - EgradZ / 3. - VpotZ
    res['p'] = res['p0']+res['pWp']+res['pWm']+res['pZ']
    
    res['rho0'] = Ekin0 + Egrad0 + Vpot0
    res['rhoWp'] = EkinWp + EgradWp + VpotWp
    res['rhoWm'] = EkinWm + EgradWm + VpotWm
    res['rhoZ'] = EkinZ + EgradZ + VpotZ
    res['rho'] = res['rho0']+res['rhoWp']+res['rhoWm']+res['rhoZ']
    res['rhofromtxt'] = rho
    
    return t, res 

import  matplotlib.pyplot as plt
from    matplotlib.widgets    import  Slider

class SpectraOutput():
    def __init__(self, filename, times, t0=0):
        self.times = np.loadtxt(times) + t0
        self.Ntimes = self.times.size
        self.fieldfluctuationsarray = np.loadtxt(filename)
        
        self.Nlabels   = self.fieldfluctuationsarray.shape[0] // self.Ntimes
        self.modelabel = self.fieldfluctuationsarray[:self.Nlabels, 0]
        self.densities, self.fieldfluctuations = [], []
        for i in range(self.Ntimes):
            b,e = i*self.Nlabels, (i+1)*self.Nlabels
            density = self.fieldfluctuationsarray[b:e, 3]
            self.densities.append(density)
            fieldfluct =  self.fieldfluctuationsarray[b:e, 1]
            self.fieldfluctuations.append(fieldfluct)

    def plot(self, xscale=None, yscale=None):
        """
        PLOTTING THE USEFUL INFORMATION
        """
        
        fig, (ax1, ax2) = plt.subplots(1,2)
        plt.title('field fluctuation')
    
    
            
        times = self.times
        Ntimes = self.Ntimes
        fieldfluctuationsarray = self.fieldfluctuationsarray
        
        Nlabels   = fieldfluctuationsarray.shape[0] // Ntimes
        modelabel = fieldfluctuationsarray[:Nlabels, 0]
        densities, fieldfluctuations = [], []
        for i in range(Ntimes):
            b,e = i*Nlabels, (i+1)*Nlabels
            density = fieldfluctuationsarray[b:e, 3]
            densities.append(density)
            fieldfluct =  fieldfluctuationsarray[b:e, 1]
            fieldfluctuations.append(fieldfluct)
            # color = f"{0.25 + 0.5 * i/Ntimes:.03f}"
            # ax1.semilogy(modelabel, density, label=f"{times[i]:.03f}", color=color)
            # ax2.plot(modelabel, fieldfluct, label=f"{times[i]:.03f}", color=color)
        global result, uvs, t1, seq
        
        axcolor   = 'lightgoldenrodyellow'
        i = 0
        axeq      = plt.axes([0.15, 0.87, 0.23, 0.015], facecolor=axcolor)
        seq       = Slider(axeq, 'time', 0, Ntimes-1, valinit=0, valstep=1)
        seq.valtext.set_visible(False)
        t1        = plt.text(0.5, 1.2, f'time: {times[i]:.03f}')
        ini       = densities[i]
        ini[np.isnan(ini)] = np.nanmin(fieldfluctuationsarray[:, 3])
        plot1,    = ax1.semilogy(modelabel, ini, lw=2, color='gray')
        plot2,    = ax2.semilogy(modelabel, fieldfluctuations[i], lw=2, color='gray')
        ax1.set_title("densities")
        ax2.set_title("scalar fluctuations")
        mi,ma = np.nanmin(fieldfluctuationsarray[:, 1]), np.nanmax(fieldfluctuationsarray[:, 1])
        ax2.set_ylim((mi, ma))
        mi,ma = np.nanmin(fieldfluctuationsarray[:, 3]), np.nanmax(fieldfluctuationsarray[:, 3])
        if xscale:
            ax1.set_xscale(xscale)
            ax2.set_xscale(xscale)
        if yscale:
            ax1.set_yscale(yscale)
            ax2.set_yscale(yscale)
        try:
            ax1.set_ylim((mi, ma))
        except ValueError:
            pass
        def press(event):
            try:
                button = event.button
            except:
                button = 'None'
            if event.key == 'right' or button == 'down':
                if seq.val < len(times) - 1:
                    seq.set_val(seq.val + 1)
            elif event.key == 'left' or button == 'up':
                if seq.val > 0:
                    seq.set_val(seq.val - 1)
            eqi0 = int(seq.val)
            t1.set_text(f'time: {times[eqi0]:.03f}')
            # im.set_data(dic_of_images[datetimes[indexvalue]])
            update(seq.val)
            fig.canvas.draw_idle()
    
        def reset(event):
            seq.reset()
        def update(val):
            eqi0 = int(seq.val)
            t1.set_text(f'time: {times[eqi0]:.03f}')
            plot1.set_ydata(densities[eqi0])
            plot2.set_ydata(fieldfluctuations[eqi0])
            fig.canvas.draw_idle()
    
        plt.legend(frameon=False)
        
        
    
        plt.legend(frameon=False)
        plt.tight_layout()
        fig.canvas.mpl_connect('key_press_event', press)
        fig.canvas.mpl_connect('scroll_event', press)
        seq.on_changed(update)
        plt.show()