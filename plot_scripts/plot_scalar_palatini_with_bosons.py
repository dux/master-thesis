#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  1 17:26:18 2021

@author: fred
"""
output = "../../lattice_scripts_output/scalar_higgspalatini/scalar_higgs_palatini_bosons_N256_kIR0.007/"

import matplotlib as mpl

import matplotlib.pyplot as plt
import numpy as np

from utilities import loadScalarMetric, SpectraOutput

eta, r = loadScalarMetric(output)
#%%

xi = 1e7
lambd = 0.001
Mp =  2.435e18
fStarb      = Mp / xi**0.5
omegaStarb  = 0.5 * fStarb;

fStar =  Mp

omegaStar = lambd**0.5 / (2*xi) * Mp

t = eta / omegaStarb * omegaStar 
r['phiL'] = r['phiL'] * fStarb / fStar
#%%

# t, [phiL, phiLd, phiL2, phiLd2, varphiL, varphiLd], \
#    [a, aDot, HL], \
#    Ekin0, Egrad0, Vpot0, rho0, p0 = loadScalarMetric(output, 0)
# t, [WpL, WpLd, WpL2, WpLd2, varWpL, varWpLd], \
#    [a, aDot, HL], \
#    EkinWp, EgradWp, VpotWp, rhoWp, pWp = loadScalarMetric(output, 1)
# t, [WmL, WmLd, WmL2, WmLd2, varWmL, varWmLd], \
#    [a, aDot, HL], \
#    EkinWm, EgradWm, VpotWm, rhoWm, pWm = loadScalarMetric(output, 2)
# t, [ZL, ZLd, ZL2, ZLd2, varZL, varZLd], \
#    [a, aDot, HL], \
#    EkinZ, EgradZ, VpotZ, rhoZ, pZ = loadScalarMetric(output, 3)

# ptot = p0 + pWp + pWm + pZ
# rhotot = rho0 + rhoWp + rhoWm + rhoZ
# 
#%% plot of the behaviour of the field at the beginning
if 1:
    plt.style.use('masterthesis.mplstyle')
    
    tmin, tmax = -0.002, 0.03
    fig, (ax1,ax2) = plt.subplots(1,2, figsize=(6,3.3))
    taus, phi = np.load('scalar_palatini_homogeneous.npy')
    ax1.plot(t, r['phiL'], label=r'Lattice $\langle \chi \rangle(t)$')
    ax1.plot(taus, phi, ls='--', lw=0.8, dashes=(1, 1), label=r'$\chi(t)$ from eq. (\ref{eq:chiunitaryeomhomogeneousrescaled})'
    ax1.set_xlabel(r'Time $\tilde{t}/2\pi$')
    ax1.set_ylabel(r'Field values')
    ax1.set_xlim((tmin, tmax))
    ax1.set_ylim((-0.001,0.001))
    # ax1.set_ylim((-0.00117, 0.00163))
    ax1.legend()
    
    ax2.semilogy(t, r['a']**3*r['Egradphi']/r['rho'][0], label=r"$a^3 E_\mathrm{grad}({\chi}) / \rho_0$")
    ax2.semilogy(t, r['a']**3*r['rhoWp']/r['rho'][0], label=r"$a^3 \rho_{W^+} / \rho_0$")
    ax2.semilogy(t, r['a']**3*r['rhoWm']/r['rho'][0], label=r"$a^3 \rho_{W^-} / \rho_0$")
    ax2.semilogy(t, r['a']**3*r['rhoZ']/r['rho'][0], label=r"$a^3 \rho_{Z_0} / \rho_0$")
    ax2.set_xlim((tmin, tmax))
    ax2.set_xlabel(r'Time $\tilde{t}/2\pi$')
    ax2.legend(handlelength=1)
    plt.tight_layout()
    # mpl.use('pgf')
    # fig.savefig('../figures/scalarpalatinibosonsbeginning.pgf')
    
#%% plot the spectra
def latex_float(f):
    if f == 0:
        return '0'
    float_str = "{0:.0f}".format(f)
    if "e" in float_str:
        base, exponent = float_str.split("e")
        return r"{0} \cdot 10^{{{1}}}".format(base, int(exponent))
    else:
        return float_str
#%%
if 0:
    spectraWp = SpectraOutput(output + 'spectra_scalar_1.txt', 
                            output+'average_spectra_times.txt')
    spectraWm = SpectraOutput(output + 'spectra_scalar_2.txt', 
                            output+'average_spectra_times.txt')
    spectraZ = SpectraOutput(output + 'spectra_scalar_3.txt', 
                            output+'average_spectra_times.txt')
    plt.style.use('masterthesis.mplstyle')
    #%%
    fig, (ax1,ax2) = plt.subplots(1,2, figsize=(6,3.3))

    for ti in [2, 6.5, 7.1, 15, 25][::-1]:
        sample = np.argmin(np.abs(spectraWp.times/(2*np.pi)-ti))
        label = rf"${latex_float(spectraWp.times[sample]/(2*np.pi))}$"
        ax1.semilogx(spectraWp.modelabel, r['a'][sample]**3 * spectraWp.fieldfluctuations[sample], label=label)
        ax2.semilogx(spectraZ.modelabel,  r['a'][sample]**3 * spectraZ.fieldfluctuations[sample], label=label)
    ax1.axvline(49.6, ls=':', color='gray', lw=1)
    ax2.axvline(49.6, ls=':', color='gray', lw=1)
    extraticks = [49.6]
    extraticklabel =[r'$k_*$']
    ax1.set_xticks(list(ax1.get_xticks()) + extraticks)
    ax2.set_xticks(list(ax2.get_xticks()) + extraticks)
    fig.canvas.draw()
    labs  = [item.get_text() for item in ax1.get_xticklabels()]
    labs[-1] = r'$k_*$'
    ax1.set_xticklabels(labs)
    labs  = [item.get_text() for item in ax2.get_xticklabels()]
    labs[-1] = r'$k_*$'
    ax2.set_xticklabels(labs)
    
    ax1.set_xlim((np.min(spectraZ.modelabel), 1.5*np.max(spectraZ.modelabel)))
    ax2.set_xlim((np.min(spectraZ.modelabel), 1.5*np.max(spectraZ.modelabel)))
    ax1.set_ylabel(r"$\tilde{k}^3 n_\kappa (W^{\pm})$")
    ax1.set_xlabel(r"$\tilde{k}$")
    ax1.legend(handlelength=1, title=r"$\tilde{t}/2\pi$")
    
    
    # output = "/home/fred/cosmolattice2/cosmolattice/build_higgs_palatini/output/"
    # spectra = SpectraOutput(output + 'spectra_scalar_0.txt', 
                            # output+'average_spectra_times.txt')
    # for sample in [50, 100, 150]:
        # label = f"$t={latex_float(spectra.times[sample])}$"
        # ax2.plot(spectra.modelabel, spectra.fieldfluctuations[sample], label=label)
    ax2.set_ylabel(r"$\tilde{k}^3 n_\kappa (Z_0)$")
    ax2.set_xlabel(r"$\tilde{k}$")
    ax2.legend(handlelength=1, title=r"$\tilde{t}/2\pi$")
    plt.tight_layout()        
    # mpl.use('pgf')
    # fig.savefig('../figures/scalarpalatinibosonsspectra.pgf')        
                                 
    
#%% plot the equation of state and scale factor
if 1:
    # plt.style.use('masterthesis.mplstyle')
    tmin, tmax = -5,400
    plt.style.use('masterthesis.mplstyle')
    fig, (ax1,ax2) = plt.subplots(1,2, figsize=(6,3.3))

    ax1.semilogy(t, r['a'], label=r'scale factor $a(\tilde{t})/a_0$')
    ax1.set_xlabel(r'Time $\tilde{t}$')
    ax1.set_xlim((tmin, tmax))
    ax1.legend()
    
    ax2.plot(t, r['p']/r['rho'], label=r"$w(t) = p(t)/\rho(t)$")
    ax2.axhline(y=0.3333333, ls='--', lw=1.5, color='gray', zorder=1000)
    # plt.xlim((np.min(t), np.max(t)))
    # plt.ylim((-1.03, 0.95))
    ax2.set_xlim((tmin, tmax))
    ax2.set_xlabel(r'Time $\tilde{t}$')
    ax2.legend()
    plt.tight_layout()
    # mpl.use('pgf')
    # fig.savefig('../figures/scalarpalatinibosonseqstateandscalefactor.pgf')      
    plt.show()
    