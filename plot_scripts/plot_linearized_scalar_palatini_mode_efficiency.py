#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  8 14:00:35 2021

@author: fred
"""

import  numpy                as      np
import  matplotlib.pyplot    as      plt
from    scipy.integrate      import  odeint 
import  matplotlib           as      mpl



# time span for the inflation part:
end  = 1 # was found by trial and error to be just after the end of inflation.
N    = 600
taus = np.linspace(0, 0.06, N)
# Planck mass:
Mp   = 2.435e18 # GeV


###############################################################################
######### model dependent params ##############################################
###############################################################################

lambd =  1e-3
xi    =  1e7




fstar = Mp
omegastar = lambd**0.5 / (2*xi) * Mp



def V(phi):
    tanarg = xi**0.5 * phi
    return  np.tanh( tanarg )**4

def Vp(phi):
    tanarg = xi**0.5 * phi
    num = 4 * xi**0.5 * np.tanh( tanarg )**3
    den = np.cosh( tanarg )**2
    return num / den

def Vpp(phi):
    arg = xi**0.5 * phi
    num = 4 * xi * (4 - np.cosh(2*arg)) * np.tanh(arg)**2
    den = np.cosh( arg )**4
    return num / den

def HubbleRate(phi, phidot):
    H2 = ( V(phi) + 0.5 * phidot**2 ) / 3 
    return  np.sqrt(H2)

###############################################################################
########## equation of motion (Klein-Gordon) ##################################
###############################################################################
def EOM(u, t):
    # enter phidot, phi:
    phidot, phi, qdot, q, a  = u
    H            = HubbleRate(phi, phidot)
    adot         = a * H
    phiddot      = -3 *  H * phidot - Vp(phi)
    # return the derivatives of phidot, phi:
    qddot        = - 3 * H * qdot - ( k**2 / a**2 + Vpp(phi) ) * q
    return [phiddot , phidot, qddot, qdot, adot]

###############################################################################
########## initial conditions (slow-roll) #####################################
###############################################################################
# """
# start where the potential dominates: large phi.
phi0    =  0.0014

# during inflation, slow roll: phiddot = 0. 
# then: phidot = - V'(phi) / (3 H)
# and also, phidot negligible compared to phi, set it to 0:
phidot0 =  0#-Vprime(phi0) / (3 * HubbleRate(phi0, 0)) / fstar
# ks = np.logspace(2.2,3., num=60)
ks = [500]
# ks = [500]
maxs = []
intplot = 0
qs = []
estqs = []
for k in ks:
    ###############################################################################
    ########## integrate along the time ###########################################
    ###############################################################################
    q0      = 1 / k**0.5
    qdot0   = k * q0
    sol     = odeint(EOM, [phidot0, phi0, qdot0, q0, 1], taus)
    
    ###############################################################################
    ########## find the end of inflation ##########################################
    ###############################################################################
    phidot, phi     = sol[:,0], sol[:,1]
    qdot, q         = sol[:,2], sol[:,3]
    qs.append(q)
    a               = sol[:,4]
    # get the hubble rate everywhere:
    H               = HubbleRate(phi, phidot)
    

    estq = (k**2 / a**2 + np.abs(Vpp(phi)))*q**2 + qdot**2
    # estqs.append(estq)
    estq = np.abs(q)/q[0]
    maxs.append(estq[-1])
    if intplot:
        plt.figure()
        ax1 = plt.gca()
        ax2 = ax1.twinx()
        # ax1.plot(taus, -(k**2 / a**2 + Vpp(phi)))
        ax1.plot(taus, phi)
        # estq = np.abs(q)
        ax2.plot(taus, np.log10(estq), color='darkorange')
        # ax2.set_ylim((7,27))
        ax2.set_ylabel('log10(max amplitude)')
        plt.waitforbuttonpress()
#%%
# """
if 0:
    plt.style.use('masterthesis.mplstyle')
    fig, (ax1,ax2) = plt.subplots(1,2, figsize=(6,3.3))
    choice = 1
    ax1.semilogy(taus, np.abs(qs[choice])/qs[choice][0], label=f"$\\vert \\delta\\tilde{{\\chi}}_{{\\tilde{{k}}}}(\\tilde{{t}}) / \\delta\\tilde{{\\chi}}_{{\\tilde{{k}}}}(0) \\vert$ at $\\tilde{{k}}={ks[choice]:.0f}$")
    ax1.set_xlabel(r"$\tilde{t}$")
    ax1.set_ylim((1e-2, 1e14))
    ax1.legend()
    choice = 37
    ax1.semilogy(taus, np.abs(qs[choice])/qs[choice][0], label=f"$\\vert \\delta\\tilde{{\\chi}}_{{\\tilde{{k}}}}(\\tilde{{t}}) / \\delta\\tilde{{\\chi}}_{{\\tilde{{k}}}}(0) \\vert$ at $\\tilde{{k}}={ks[choice]:.0f}$")
    choice = 55
    ax1.semilogy(taus, np.abs(qs[choice])/qs[choice][0], label=f"$\\vert \\delta\\tilde{{\\chi}}_{{\\tilde{{k}}}}(\\tilde{{t}}) / \\delta\\tilde{{\\chi}}_{{\\tilde{{k}}}}(0) \\vert$ at $\\tilde{{k}}={ks[choice]:.0f}$")
    ax1.legend(handlelength=0.8)
    
    ax2.semilogx(ks, maxs, label=r'$\delta\tilde{\chi}_{\tilde{k}}(\tilde{t}=0.06)/\delta\tilde{\chi}_{\tilde{k}}(\tilde{t}=0)$')
    ax2.set_xlabel(r'$\tilde{k}$')
    ax2.set_ylim((-1e9, 1.15e12))
    # ax2.set_xticks(list(ax2.get_xticks()))
    ax2.legend()
    
    plt.tight_layout()
    # mpl.use('pgf')
    # fig.savefig('../figures/palatini_scalar_mode_efficiency.pgf')      
#"""