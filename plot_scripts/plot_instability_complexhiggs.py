#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun  6 18:32:22 2021

@author: fred
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
plt.style.use('masterthesis.mplstyle')

taus, H = np.load('complexhiggsinstablehomogeneous.npy')
t, HL, Egrad, Eint, EHubble = np.load('complexhiggsinstable.npy')

fig, (ax1,ax2) = plt.subplots(1,2, figsize=(6,3.))


ax1.plot(t, HL, label=r"lattice average $\langle \mathrm{Im}\, \tilde{H}\rangle (\tilde{t})$", lw=2)
ax1.plot(taus, H, ls=':', label=r"Homogeneous case")
ax1.set_ylim((-1.1*H[0], 1.1*H[0]))
ax1.set_xlabel(r"$\tilde{t}$")
ax1.set_xlim((-0.001, 0.034))
ax1.legend()


ax2.semilogy(t, Egrad/(Eint[0]+EHubble[0]), label=r"$\left\langle\frac{1}{1+2 \xi  \vert \tilde{H} \vert^2} \vert \nabla \tilde{H} \vert ^2 \right\rangle/ \tilde{\rho}(0)$")
ax2.set_ylim((1e-9, 5))
ax2.set_xlabel(r"$\tilde{t}$")
ax2.set_xlim((-0.001, 0.034))
ax2.legend()

plt.tight_layout()
fig.savefig('../../meetings_com/2021_07_15_defense/u1instability.svg', transparent=True)

# mpl.use('pgf')

# plt.savefig('../figures/h_nonminimalcoupling_instability.pgf')