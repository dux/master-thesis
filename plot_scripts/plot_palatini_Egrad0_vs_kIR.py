#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  7 13:48:12 2021

@author: fred
"""

# so I was stupid and didn't save the data correctly.
# since those were expensive calculations in a for loop, didn't want to redo
# them.

# I saved the matplotlib graph instead in a pickle, which contains all the data.
# load it here

import pickle
import matplotlib.pyplot as plt

plt.style.use('masterthesis.mplstyle')
ax = pickle.load(open('Egrad_vs_kIR.pickle', 'rb'))

kIRs = [380, 400, 420, 440, 450, 460, 480]
# ax.legend(title=r"$\tilde{k}_{\mathrm{IR}}$")
Egradsdividedbyrhoini = []
for l in list(ax.get_lines()):
    t = l.get_xdata()
    Egradsdividedbyrhoini.append(l.get_ydata().copy())
plt.close() 
#%%
fig, (ax1, ax2) = plt.subplots(1,2,figsize=(6,3.3))


for kIR, grad in zip(kIRs, Egradsdividedbyrhoini):
    ax1.semilogy(t, grad, label=kIR)
    
    ax2.semilogy(t, grad, label=kIR)


ax2.set_xlim((0.025, 0.03))
ax2.set_ylim((5e-2, 0.4))



    
ax1.set_xlabel(r"$\tilde{t}$")
ax2.set_xlabel(r"$\tilde{t}$")
ax1.set_ylabel(r"$E_{\mathrm{grad}}(\tilde{\chi}) / \tilde{\rho}(0)$")
ax2.set_ylabel(r"$E_{\mathrm{grad}}(\tilde{\chi}) / \tilde{\rho}(0)$")
ax1.legend(title=r"$\tilde{k}_{\mathrm{IR}}$")
ax2.legend(title=r"$\tilde{k}_{\mathrm{IR}}$")

plt.tight_layout()

import matplotlib as mpl
# mpl.use('pgf')
# fig.savefig('../figures/palatini_optimal_kir.pgf')