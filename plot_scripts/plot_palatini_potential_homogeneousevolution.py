#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 29 14:27:05 2021

@author: fred
"""

import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
plt.style.use('masterthesis.mplstyle')


Lambd = 0.001
xi = 1e7 



def palpotrescaled(chi):
    return np.tanh(xi**0.5 * chi)**4


chis = np.linspace(-0.0015, 0.0015, 150)
fig, (ax1,ax2) = plt.subplots(1,2, figsize=(6,2.7))#plt.figure()
taus, phi = np.load('tausphioscillates.npy')

ax1.plot(chis, palpotrescaled(chis))
ax1.set_xlabel(r"$\tilde{\chi}$")
ax1.set_ylabel(r"$\tilde{V}(\tilde{\chi})$")
ax1.set_xlim((-0.0015, 0.0015))

ax2.plot(taus, phi)
ax2.set_xlabel(r"$\tilde{t}$")
ax2.set_ylabel(r"$\tilde{\chi}(\tilde{t})$")
ax2.set_xlim((-0.001, 0.145))

plt.tight_layout()
# mpl.use('pgf')
# fig.savefig('../figures/potentialandtauschioscillates.pgf')