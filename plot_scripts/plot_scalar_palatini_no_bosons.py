#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 28 18:51:36 2021

@author: fred
"""
output = "../../lattice_scripts_output/scalar_higgspalatini/scalar_higgs_palatini_N=256_kIR=80_save/"
output = "/home/fred/outputnoboson_256_kIR80_up2/"
# output = '../../lattice_scripts_output/scalar_higgspalatini/scalar_higgs_palatini_N=256_kIR=80_long_times/'
# output = "/home/fred/cosmolattice2/cosmolattice/build_higgs_palatini/output/"

import matplotlib as mpl

import matplotlib.pyplot as plt
import numpy as np

from utilities import loadScalarPalatiniNoBosons, SpectraOutput



t, [phiL, phiLd, phiL2, phiLd2, varphiL, varphiLd], \
   [a, aDot, HL], \
   Ekin0, Egrad0, Vpot0, rho, p0 = loadScalarPalatiniNoBosons(output)
   
#%% observables

temps =[]
def Tre(rhoco, gre, weff, Nre):
    pref = (30 * rhoco / ( gre * pi**2))**0.25
    exp = np.exp(-0.75 * (1 + weff) * Nre)
    return pref * exp * (omegastar**2 * fstar**2)**0.25
import numpy as np
trs = [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000]
for tr in trs:
    tr = tr * 20
    w = p0 / rho
    pi = np.pi
    gre = 106.75
    Mp   = 2.435e18
    xi   = 1e7
    lambd = 0.001
    da = np.diff(a)
    da = np.append(da, da[-1])
    weff = np.sum(da[:tr]/a[:tr] * w[:tr]) / np.log(a[-1])
    print(tr, weff, end = ' ')
    Nre = np.log(a[tr])
    rhoco = 0.99
    
    fstar = Mp
    omegastar = lambd**0.5 / (2*xi) * Mp
    

    
    Treinstant = (30 * lambd / ( 4 * pi**2 * xi**2 * gre))**0.25 * Mp
    TreLat = Tre(rhoco, gre, weff, Nre)
    
    # plt.figure(); plt.plot(t, w); plt.axvline(t[tr])
    print(TreLat / Treinstant)
    temps.append(TreLat / Treinstant)
plt.plot(trs, temps)
# 
#%% plot of the behaviour of the field at the beginning
if 1:
    plt.style.use('masterthesis.mplstyle')
    tmin, tmax = -0.002, 0.042
    fig, (ax1,ax2) = plt.subplots(1,2, figsize=(6,3.3))
    taus, phi = np.load('scalar_palatini_homogeneous.npy')
    ax1.plot(t, phiL, label=r'Lattice $\langle \tilde{\chi} \rangle(\tilde{t})$')
    ax1.plot(taus, phi, ls='--', lw=1, label=r'$\tilde{\chi}(\tilde{t})$ from eq. (\ref{eq:chiunitaryeomhomogeneousrescaled})')
    ax1.set_xlabel(r'Time $\tilde{t}$')
    ax1.set_ylabel(r'Field values')
    ax1.set_xlim((tmin, tmax))
    ax1.set_ylim((-0.0013, 0.00178))
    ax1.legend()
    
    ax2.semilogy(t, a**3*Egrad0/rho[0], label=r"$a^3 E_\mathrm{grad} / \rho(t=0)$")
    ax2.set_xlim((tmin, tmax))
    ax2.set_xlabel(r"Time $\tilde{t}$")
    ax2.legend()
    plt.tight_layout()
    # mpl.use('pgf')
    # fig.savefig('../figures/scalarpalatinibeginning.pgf')
    
#%% plot the spectra
def latex_float(f):
    if f == 0:
        return '0'
    float_str = "{0:.0e}".format(f)
    if "e" in float_str:
        base, exponent = float_str.split("e")
        return r"{0} \cdot 10^{{{1}}}".format(base, int(exponent))
    else:
        return float_str
    
if 1:
    output = "../../lattice_scripts_output/scalar_higgspalatini/scalar_higgs_palatini_N=256_kIR=80_save/"
    spectra = SpectraOutput(output + 'spectra_scalar_0.txt', 
                            output+'average_spectra_times.txt')
    plt.style.use('masterthesis.mplstyle')
    fig, (ax1,ax2) = plt.subplots(1,2, figsize=(6,3.3))

    for sample in [1, 2, 3]:
        label = rf"$\tilde{{t}}={latex_float(spectra.times[sample])}$"
        ax1.plot(spectra.modelabel, spectra.fieldfluctuations[sample], label=label)
    ax1.set_ylabel(r"$\tilde{k}^3 n_{\tilde{k}} (\chi)$")
    ax1.set_xlabel(r"$\tilde{k}$")
    ax1.legend()
    
    
    # output = "/home/fred/cosmolattice2/cosmolattice/build_higgs_palatini/output/"
    # spectra = SpectraOutput(output + 'spectra_scalar_0.txt', 
                            # output+'average_spectra_times.txt')
    for sample in [50, 100, 150]:
        label = rf"$\tilde{{t}}={latex_float(spectra.times[sample])}$"
        ax2.plot(spectra.modelabel, spectra.fieldfluctuations[sample], label=label)
    ax2.set_ylabel(r"$\tilde{k}^3 n_{\tilde{k}} (\chi)$")
    ax2.set_xlabel(r"$\tilde{k}$")
    ax2.legend()
    plt.tight_layout()        
    # mpl.use('pgf')
    # fig.savefig('../figures/scalarpalatinispectra.pgf')        
                                 
    # plt.plot(t, p0/rho)
    
#%% plot the equation of state 
if 1:
    # output = '../../lattice_scripts_output/scalar_higgspalatini/scalar_higgs_palatini_N=256_kIR=80_long_times/'
    # t, [phiL, phiLd, phiL2, phiLd2, varphiL, varphiLd], \
    #    [a, aDot, HL], \
    #    Ekin0, Egrad0, Vpot0, rho, p0 = loadScalarPalatiniNoBosons(output)
    plt.style.use('masterthesis.mplstyle')
    # plt.style.use('masterthesis.mplstyle')
    tmin, tmax = -0.01,2.01
    plt.style.use('masterthesis.mplstyle')
    fig, (ax1,ax2) = plt.subplots(1,2, figsize=(6,2.8))

    ax1.plot(t, a, label=r'scale factor $a(\tilde{t})/a_0$')
    ax1.set_xlabel(r'Time $\tilde{t}$')
    ax1.set_xlim((tmin, tmax))
    ax1.legend()
    
    ax2.plot(t, p0/rho, label=r"$w(\tilde{t}) = \tilde{p}(\tilde{t})/\tilde{\rho}(\tilde{t})$")
    
    ax2.axhline(y=0.33, ls='--', lw=1.5, color='gray', zorder=1000)
    # plt.xlim((np.min(t), np.max(t)))
    # plt.ylim((-1.03, 0.95))
    ax2.set_xlim((tmin, tmax))
    ax2.set_xlabel(r'Time $\tilde{t}$')
    ax2.legend()
    plt.tight_layout()
    # mpl.use('pgf')
    fig.savefig('../figures/scalarpalatinieqstateandscalefactor.pgf')      
    plt.show()
    