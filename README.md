# How to use:

The contents of the report should be edited using [lyx](https://www.lyx.org/). The corresponding files are in the `lyxfiles` directory. 

A makefile and some scripts then put everything together. The `make` command executes the following actions:

- convert the `lyx` documents to `tex` files,
- extract the relevant content in each `tex` file, which are inputted in `main.tex`.
- compile the figures,
- compile the main document in the `build` subdirectory. 



